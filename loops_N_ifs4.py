#! /opt/homebrew/Caskroom/miniconda/base/bin/python
#####################
#      Samples      #
#####################
import sys
from functools import reduce

# sum up the numbers between 1 and 1000
print(sum(range(1,1001)))

# same with comprehension
print(sum([i for i in range(1,1001)]))

# do it with a for loop
sum = 0
for i in range(1,1001):
    sum += i
print(sum)

# do it with a while loop
sum = 0
i = 1
while i < 1001:
    sum += i
    i += 1
print(sum)

# with lambda and reduce
print(reduce(lambda x,y: x+y, range(1,1001)))

# same with cast to list at range
print(reduce(lambda x,y: x+y, list(range(1,1001))))

# getting a multiplication table
for i in range(1,11):
    for j in range(1,11):
        print('%dx%d=%d' % (i,j,i*j), end=" ")
    print()

# working with parameters from sys

# check if there are arguments
if len(sys.argv) > 1:
    # if there are, print them with for loop
    for i in range(1,len(sys.argv)):
        print(sys.argv[i])
    # print them without range but with for loop
    for i in sys.argv[1:]:
        print(i)
# else print a message no args
else:
    print("No arguments")

