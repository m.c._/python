from random import randint, random, uniform, sample

x = 3
y = 4

# gets the division with out being a float
print(3//4)

# convert to octal number value
octnumb = 0o750

print(octnumb)
intnumb = int(octnumb)

print(intnumb)

# this will give you random numbers between 1 and 49
print(randint(1, 49))

# this will give you random numbers between 0 and 0.9xxxx
print(random())

# this will give you random numbers between 1.0 and 49.5
print(uniform(1.0, 49.5))

# getting to random numbers between 0.0 andf 10.0
random_numbers = sample(range(0, 100), 2)
random_numbers = [num / 10 for num in random_numbers]
print(random_numbers)


random_numbers = [uniform(0.0, 10.0) for _ in range(2)]
print(random_numbers)

# This code generates a list of two random floats between 0.0 and 10.0 using the random.uniform function. The _ is used as a throwaway variable to indicate that we don't care about the loop counter variable.