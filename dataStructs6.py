#########################
#   Type Annotations    #
#########################
from typing import List, Set, Dict, Tuple
lst: List[int]
set: Set[str]
dict: Dict[str, int]
tuple: Tuple[int, str]
lst: List[int] = [1, 2, 3]