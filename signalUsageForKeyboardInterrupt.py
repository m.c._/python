import signal, sys

def cancel(signal, frame):
    print("\nYou pressed Ctrl+C!")
    sys.exit(0)

signal.signal(signal.SIGINT, cancel)

cnt = 0
while 1:
    cnt += 1
    print(cnt)