try:
    # read test.txt
    f = open('text.txt')
    # read all lines and print
    for line in f:
        print(line, end='')
except FileNotFoundError:
    print('File not found')
except KeyboardInterrupt:
    pass # this does nothing when user hits control + c
except BaseException as err:
    print('Error occurred: ', err) # this is like catch all with information about the error
except:
    print('Error occurred')

finally:
    if 'f' in locals() and f:
        f.close()