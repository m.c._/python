####################
#      params      #
####################

# sample for params immutable
def params_immutable(a, b):
    a = a + 1 # this will not change the value of the variable
    print(a, b)

# sample for params mutable
def params_mutable(a, b):
    b.append(1) # this will change the value of the list
    print(a, b)

# mutable but with init in function
def params_mutable_init(a, b=[]):
    b = [4, 8] # this will change the value of the list only in function!
    print(a, b)

# function with optional params
def optional_params(a, b=None, c = -1, d= 0):
    if b is None:
        b = []
    print(a, b, c, d)

# variable params count
def variable_params_count(a, b, *args):
    print(a, b, args)

# same but **args
def variable_params_count_kwargs(a, b, **kwargs):
    print(a, b, kwargs) # this builds a dict

# function that converts dicts in to params  
def f(a,b, **c):
    print(a,b,c)

# force params to be on a special order
def force_params_order(a, b, c, d):
    print('a=', a, 'b=', b, 'c=', c, 'd=', d, sep=' ')

# use / for params
def use_slash_for_params(a, b, c, d, /):
    print('a=', a, 'b=', b, 'c=', c, 'd=', d, sep=' ')

# check for type of params
def check_type_of_params(a, b):
    if(isinstance(a, int) and isinstance(b, int)):
        print('a and b are ints')

    elif(isinstance(b, list)):
        print('b is a list')
        lst = range(5,9)
        for i in lst:
            b.append(i)
        print('b is a list')

    else:
        print('a and b are not ints')

# type annotation for function
def type_annotation_for_function(a: int, b: list) -> list:
    print(a, b)
    b.append(a)
    print(a, b)
    return b

# init
a = 5
b = list(range(5))

# call functions
params_immutable(a, b)

params_mutable(a, b) 
params_mutable_init(a, b)

optional_params(a, b)
optional_params(a)

optional_params(a, b, c=2)
optional_params(a, b, d=2)

variable_params_count(a, b, *list(range(1,9)))
variable_params_count(a, b)

variable_params_count_kwargs(a, b, **{'c':1, 'd':2})
variable_params_count_kwargs(a, b)

force_params_order(a, b, c=1, d=2)
force_params_order(a, b, d=2, c=1)

use_slash_for_params(a, b, 1, 2)
use_slash_for_params(a, b, 2, 1)

check_type_of_params(a, b)

type_annotation_for_function("2", b)
lstType = type_annotation_for_function(a, b)

print('type_annotation_for_function returns: ', lstType)

dct = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'x': 90}

f(**dct)

print(b)
print(*b) # transforms list to single values!

print(a, b)