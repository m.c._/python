#################
#	lambdas 	#
#################

# normal functions
def f1(x, y):                    
  return (x+1) * (y+1)

lst1 = [1, 2, 3, 9, 345, 36, 33]
 # lst2 should only contain all elements divisble by 3 of lst1
lst2 = list(filter(lambda x: x%3==0, lst1))
print(lst2)  # printout [3, 9, 345, 36, 33]

# now we divide the elements of lst2 by 3
lst3 = list(map(lambda x: x//3, lst2))
print(lst3) # printout [1, 3, 115, 12, 11]

# same as f1 but lambda function
f2 = lambda x, y: (x+1) * (y+1)

print(f1(2, 3))  # output 12
print(f2(2, 3))  # output 12