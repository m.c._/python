# python

In here all the code samples will be and are stored regarding python.

# this porject contains a programatic summery of the python knowlage from my obsidian repo

[[tools]]

# python

### Datatypes

- Integer and Float
- String
- Bool

### Operators

##### Python Arithmetic Operators

Arithmetic operators are used with numeric values to perform common 
mathematical operations:

| Operator | Name                  | Example |
| -------- | --------------------- | ------- |
| +        | Addition              | x + y   |
| -        | Subtraction           | x - y   |
| *        | Multiplication        | x * y   |
| /        | Division              | x / y   |
| %        | Modulus               | x % y   |
| **       | Exponentiation        | x ** y  |
| //       | Floor division        | x // y  |
| @        | Matrix multiplication | x @ y   |

---

#### Python Assignment Operators

Assignment operators are used to assign values to variables:

| Operator | Example   | Same As      |
| -------- | --------- | ------------ |
| =        | x = 5     | x = 5        |
| +=       | x += 3    | x = x + 3    |
| -=       | x -= 3    | x = x - 3    |
| \*=      | x \*= 3   | x = x * 3    |
| /=       | x /= 3    | x = x / 3    |
| %=       | x %= 3    | x = x % 3    |
| //=      | x //= 3   | x = x // 3   |
| \*\*=    | x \*\*= 3 | x = x \*\* 3 |
| &=       | x &= 3    | x = x & 3    |
| \|=      | x \|= 3   | x = x \| 3   |
| ^=       | x ^= 3    | x = x ^ 3    |
| \>\>=    | x >>= 3   | x = x >> 3   |
| <<=      | x <<= 3   | x = x << 3   |

##### Assignment Expression
```python
# reading from file, without assignment expression

with open('test.txt', 'rt') as txtfile:
	line = txtfile.readline()
		while line:
			print(line, end='')
			line = txtfile.readline() # reads next line

# reading from file, with assignment expressions requires python 3.8 <

with open('test.txt', 'rt') as textFile:
	while line := textFile.readline():
		print(line, end = '')
```

### Number Systems

- decimal <-> hexadecimal
```python
x = 160
print(hex(x)) # output => 0xa5

b = 0sxa5
print(b) # output => 160
```

- decimal <-> binary
```python
x = 7
print(bin(7)) # output => 0b111

b = 0b001101
print(b) # output => 13
```

- decimal <-> octal
```python
x = 17
print(oct(x)) # output => 0o21

b = 0o34
print(b) # output => 28
```

### Creating  random numbers

```python
from random import randint, random, uniform, sample

# this will give you random numbers between 1 and 49
print(randint(1, 49))

# this will give you random numbers between 0 and 0.9xxxx
print(random())

# this will give you random numbers between 1.0 and 49.5
print(uniform(1.0, 49.5))

# getting to random numbers between 0.0 andf 10.0
random_numbers = sample(range(0, 100), 2)
random_numbers = [num / 10 for num in random_numbers]
print(random_numbers)
``` 

### Strings in python (str)

#### Slicing

string\[start:end\] this enables us to slice the strings
```python
s = 'abcdefghijklmnopqrstuvwxyz'

print(s[3]) # first 3
print(s[3:6]) # 3 to 6
print(s[:3]) # up to index 3 
print(s[3:]) # from the index 4 on
print(s[-4]) # the index 4 from string end
print(s[-4:]) # all from the index 4 from string end
```

#### Stride

With strides we can adjust the slicing even, string\[start:end:stride\]

```python
s = 'abcdefghijklmnopqrstuvwxyz'

print(s[::2]) # every second char
print(s[:10:2]) # every second char ending at the index 10
print(s[10::2]) # every second char starting at the index 10
print(s[::-1]) # all in reverse
print(s[::-2]) # every second char in reverse
print(s[:10][::-1]) # the first 10 chars in reverse
```

#### string methods
| method              | function                                             |
| ------------------- | ---------------------------------------------------- |
| len(s)              | gets the length of the string                        |
| str(someVar)        | casts the given variable in to a string              |
| ‘sub‘ in s          | test ob the substring is part of string              |
| s.count(sub)        | gets the occurance of sub in s                       |
| s.endswith(sub)     | checks if s ends with the substring                  |
| s.startswith(sub)   | checks if s starts with the substring                |
| s.expandtabs()      | replaces tabs with whitespaces                       |
| s.find(sub)         | searches for substring starts at index -1            |
| str.isxxx(s)        | checks for properties of s, e.g. islower()           |
| s.join(x)           | joins the s and x in x (list, tuple, set)            |
| s.lower()           | turns all chars in to lowercase                      |
| s.upper()           | turns all chars in to uppercase                      |
| s.partition(sub)    | splits s and returns 3 tuple                         |
| s.replace(old, new) | returns s while replacing old substring with new one |
| s.rfind(sub)        | like find but inreverse                              |
| s.split(sub)        | splits s each time the sub string occures            |
| s.splitlines()      | splits s line by line returns a list                 |
| s.strip()           | removes whitespaces and start and end of s           |

#### Formatting and converting

There are to common options to cast vars to strings
```python
from fractions import Fraction

x = Fraction('1/3') # 1/3 as a Fraction object
print(str(x)) # humanreadable
s=repr(x) # machinereadable
print(s)
y = eval(s) # converts back to object
print(y)
```

For formatting strings you can use 4 methods:

- like in c with %
```python
name = 'Mike'
age = 27
print('%s has the age %d' % (name, age)) 

floatVar = 1/7
print('1/7 as float with 3 decimal places: %.3f' % (floatVar))

source = 'image.jpg'
mode = 'landscape'
width = 200
print('<img src="%s" alt="%s" width="%d">' % (source, mode, width))
```

| code  | meaning                             |
| ----- | ----------------------------------- |
| %d    | number (decimal)                    |
| %5d   | number with 5 places, right flushed |
| %-5d  | number with 5 places, left flushed  |
| %f    | float                               |
| %.2f  | float with 2 decimal places         |
| %r    | string with using `repr`              |
| %s    | string                              |
| %10s  | string with 10 chars                |
| %-10s | string with 10 chars, left flushed  |
| %x    | number shown as hex                 |

- with the use of `format`
advantage of this variant is the free placing of the `{n}` placeholder

```python
print('{} is {} years old'.format('Mike', 98))
print('{1} is {0} years old'.format(56, 'Hank'))
print('{_name} is {_age} years old'.format(_age=age, _name=name))
print('{1} is {0} years old'.format(age, name))
print('1/7 as float with 3 decimal places: {:.3f}'.format(floatVar))
print('SELECT * FROM table WHERE id={:d}'.format(345))
```

| code   | meaning                              |
| ------ | ------------------------------------ |
| {}     | parameter, with no defined data type |
| {n}    | numbered parameters                  |
| {one}  | named parameters                     |
| {:d}   | number (decimal)                     |
| {:>5d} | number with 5 places, right flushed  |
| {:<5d} | number with 5 places, left flushed   |
| {:^7d} | number with 7 places centered        |
| {:f}   | float                                |
|  {:,f}      |float with thousands separator   |
| {.2f}  | float with 2 decimal places          |
| {:n}     | number with localization             |
| {:s}     | string with using `str               |

- using the short form of format
simple use `f` in the code
```python
print(f'{name} is {age} years old')
print(f'1/7 as float with 3 decimal places: {floatVar:.6}') # floatVar with 6 decimal places
print(f'{name=} {age=}') # new since python 3.8
```

- using localization
```python
import locale

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
stringText = '2.5'
x=locale.atof(stringText)
print(x*2)
```

#### Regex
In python there is a ready to go module to use regex, called `re`, its methods are:

| method                     | meaning                                  |
| -------------------------- | ---------------------------------------- |
| match(pattern, str)        | tests if str contains the patter         |
| search(pattern, str)       | searches for the pattern in str          |
| split(pattern, str)        | splits str each time the pattern occurs  |
| sub(pattern, replace, str) | replaces the pattern in str with replace |

With the methods above you can build regex modifications etc.

Regex tabel:

| code        | meaning                         |
| ----------- | ------------------------------- |
| ^           | beginning of string             |
| $           | end of string                   |
| .           | an undefined char               |
| \[a-z\]     | a char between a and z          |
| \[a,b,f-h\] | a char between a, b, f, g and h |
| \[^0-9\]    | a undefined char expect 0-9     |

### Date and Time

```python
# samples for working with dates
from datetime import datetime, time, timedelta
import locale

now = datetime.now()
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
print(now.strftime('%A, %d.%m.'))

s = now.strftime('%A, %d.%m.')
print(s.replace('.0', '.'))

start = time(19, 30)
startToday = datetime.combine(datetime.today(), start)
length = timedelta(minutes=132)
end = startToday + length

print(end.time())
print(end)
```


### Data structs
- Lists
```python
#####################
#      Lists        #
#####################
thislist = ["apple", "banana", "cherry"]  
print(thislist)

lst = [1, 2, 3, 'jkdsnfgkmn',"kjdnfkdfgn", 12 ]
```

1.  the `range` method of lists
		You can use this method to easily create number value containing lists
```python
# using range
# range tange(start, end, step) end is excluded !
numList = list(range(5, 200, 15))
print(numList)
```

2.  converting strings to lists
		This is done by casting the string with `list()`
```python
# casting strings to lists
strList = list('This shall be a list now!')
print(strList)
#joining list x.join x represents the separator
print(''.join(strList))
print('->'.join(strList))
```

3. comprehension
		building a new list like `[expression for x in list]`
```python
numList = list(range(10, 101, 10))
#list comprehension
print([x*2+1 for x in numList])
print([[x, x*x] for x in numList])
for x in numList:
	print(x, x*x, x**3, sep='\t')
```

- Tupel
	A tuple is a list that once defined, can not be changed.
	This can be useful for test and other handy methods. 
```python
#####################
#      Tuple        #
#####################
from random import randint

thistuple = ("apple", "banana", "cherry")  
print(thistuple) 

# create a tuple with 23, 3, 78
my_tuple = (23, 3, 78)
print(my_tuple)

# same tuple without braces
my_tuple = 23, 3, 78
print(my_tuple)

```

- Sets
```python
#####################
#       Sets        #
#####################
# Create an empty set
s = set()

# create a set with 1, 2, 3
s2 = {1, 2, 3}

# create a set witch Hello World! using set()
s3 = set('Hello World!')

# print all sets with logical formatting
print(f'Set s: {s}')
print(f'Set s2: {s2}')
print(f'Set s3: {s3}')

```

- Dictionaries
```python
#############################
#       Dictionaries        #
#############################
# A dictionary is a collection of key-value pairs.
# A dictionary is a collection which is ordered*, changeable and does not allow duplicates.
# A dictionary is written with curly brackets, and has keys and values:
# Example:

my_dict = {
	"name": "John",
	"age": 30,
	"city": "New York",
	"country": "USA",
	"is_married": False,
	"skills": ["Python", "Java", "C++"],
	"address": {
		"street": "123 Main St",
		"city": "New York",
		"state": "NY",
		"zipcode": "10001"
	}
} 

print(my_dict)
```
