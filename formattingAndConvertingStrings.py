import locale
from fractions import Fraction

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

x = Fraction('1/3') # 1/3 as a Fraction object

print(str(x)) # human readable

s=repr(x) # machine readable

print(s)

y = eval(s) # converts back to object

print(y)

# like in c with %
name = 'Mike'
age = 27
print('%s has the age %d' % (name, age)) 

floatVar = 1/7
print('1/7 as float with 3 decimal places: %.3f' % (floatVar))

source = 'image.jpg'
mode = 'landscape'
width = 200
print('<img src="%s" alt="%s" width="%d">' % (source, mode, width))

# with the use of format
print('{} is {} years old'.format('Mike', 98))
print('{1} is {0} years old'.format(56, 'Hank'))
print('{_name} is {_age} years old'.format(_age=age, _name=name))
print('{1} is {0} years old'.format(age, name))
print('1/7 as float with 3 decimal places: {:.3f}'.format(floatVar))
print('SELECT * FROM table WHERE id={:d}'.format(345))

# using f
print(f'{name} is {age} years old')
print(f'1/7 as float with 3 decimal places: {floatVar:.6f}') # floatVar with 6 decimal places
print(f'{name=} {age=}') # new since python 3.8

# using localization
stringText = '2.5'
x=locale.atof(stringText)
print(x*2)