#########################
#   Pattern Matching    #
#########################

# give a example for pattern matching
# for example:
#   a) abc
#   b) abba
lst = ["abc", "abba", "abcd", "abab"]
match lst:
    case ["abc", "abba", *_] | ["abba", "abc", *_]:
        print("Matched")
    case _:
        print("Not matched")

lst2 = ['join', 1, 2]
match lst2:
    case ['quit']:
        print("Quit")
    case ['do', cmd]:
        print(f"Doing {cmd}")
    case ['join', part1, part2]:
        print(f"Joining {part1} and {part2}")
    case ['join', *parts]:
        lst = [p for p in parts]
        print(lst)
    case _:
        print("Not matched")