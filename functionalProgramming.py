# functional programming
import math

# The `funcbuilder` and `funcbuilderlambda` functions are examples of higher-order functions in functional programming. They both take a function `f` and a number `n` as arguments, 
# and return a new function that applies `f` to `n*x`.

# Here's how the provided examples work:

# 1. `sin2 = funcbuilder(math.sin, 2)`: This line uses `funcbuilder` to create a new function `sin2(x)` that computes `math.sin(2*x)`. 
# The result is equivalent to `math.sin(3*2)` when `x=3`.

# 2. `sin3 = funcbuilderlambda(math.sin, 3)`: This line uses `funcbuilderlambda` to create a new function `sin3(x)` that computes `math.sin(3*x)`.
# The result is equivalent to `math.sin(3*3)` when `x=3`.

# 3. `cos4 = funcbuilder(math.cos, 4)`: This line uses `funcbuilder` to create a new function `cos4(x)` that computes `math.cos(4*x)`.
# The result is equivalent to `math.cos(3*4)` when `x=3`.

# 4. `cos5 = funcbuilderlambda(math.cos, 5)`: This line uses `funcbuilderlambda` to create a new function `cos5(x)` that computes `math.cos(5*x)`.
# The result is equivalent to `math.cos(3*5)` when `x=3`.

# The difference between `funcbuilder` and `funcbuilderlambda` is that `funcbuilder` defines a new function using the `def` keyword, while `funcbuilderlambda` uses a lambda function. 
# Lambda functions are anonymous functions that are defined on the fly and can be used wherever function objects are required. 
# They are syntactically restricted to a single expression and are often used for small, one-off functions.

def funcbuilder(f, n):
    def newfunc(x):
        return f(n*x) #return value of newfunc
    return newfunc #return value of funcbuilder

def funcbuilderlambda(f, n):
    return lambda x: f(n*x) #return value of funcbuilderlambda

# build the function sin(2*x)
sin2 = funcbuilder(math.sin, 2)
print(sin2(3), math.sin(3*2))
# build the function sin(3*x) with funcbuilderlambda
sin3 = funcbuilderlambda(math.sin, 3)
print(sin3(3), math.sin(3*3))

# build the function cos(4*x)
cos4 = funcbuilder(math.cos, 4)
print(cos4(3), math.cos(3*4))
# build the function cos(5*x) with funcbuilderlambda
cos5 = funcbuilderlambda(math.cos, 5)
print(cos5(3), math.cos(3*5))


