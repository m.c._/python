# In the third example, the function `buildlist` generates a list. 
# Four parameters are passed to the function: `n` determines the number of list elements. 
#`start` and `end` specify the desired value range. `fn` is finally a function itself that expects a parameter.
# A call in the form `buildlist(5, 0.0, 4.0, math.sqrt)` returns a list with five elements,
# where the first element is the square root of 0, the last is the square root of 4, 
# and the intermediate elements accordingly provide roots in the value range between 0 and 4. 
# In the second application example, `lambda x: x` is passed as a function - so a function that returns the parameter unchanged. 


import math
def buildlist(n, start, end, fn):
    if n<= 1:
        return [ ] # empty list
    delta = (end - start)/(n-1)
    return [ fn(start + i*delta) for i in range(n)]

# usage
print(buildlist(5, 0.0, 4.0, math.sqrt))

# Assuming buildlist returns a list
lst = buildlist(5, 0.0, 4.0, math.sqrt)

# Print each element in the list with 2 decimal places
for i in lst:
    print("{:.2f}".format(i))

# Assuming buildlist returns a list
lst = buildlist(5, 0.0, 4.0, math.sqrt)

# Modify each element in the list to have only 2 decimal places
rounded_lst = [round(i, 2) for i in lst]

print(rounded_lst)

# usage with lambda
print(buildlist(5, 0.0, 4.0, lambda x: x))