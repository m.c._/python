# nested functbuilder
import math

# The `nestedfuncbuilder` function is a higher-order function that takes three arguments: two functions `f` and `g`, and a number `x`. It applies the function `g` to `x`, and then applies the function `f` to the result. In other words, it computes the composition of `f` and `g` at `x`, denoted as `f(g(x))`.

# Here's how the provided examples work:

# 1. `nestedfuncbuilder(print, math.sin, 0.8)`: This line computes the sine of `0.8` using `math.sin(0.8)`, and then prints the result using `print()`. This is equivalent to `print(math.sin(0.8))`.

# 2. `nestedfuncbuilder(math.sin, math.sqrt, 0.99)`: This line first computes the square root of `0.99` using `math.sqrt(0.99)`, and then computes the sine of the result using `math.sin()`. 
# This is equivalent to `math.sin(math.sqrt(0.99))`.

# The `nestedfuncbuilder` function is a powerful tool because it allows you to compose two functions together in a single line of code. 
# This can make your code more concise and easier to read, especially when dealing with complex mathematical expressions. However, 
# keep in mind that the order of function composition matters: `f(g(x))` is not necessarily the same as `g(f(x))`. 
# Always make sure to use the correct order for your specific use case.

def nestedfuncbuilder(f,g,x):
    return f(g(x))

# use it
nestedfuncbuilder(print, math.sin, 0.8)
print(math.sin(0.8))

result = nestedfuncbuilder(math.sin, math.sqrt, 0.99)
print(result == math.sin(math.sqrt(0.99)))
