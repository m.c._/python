# Example file fibonacci.py
# Generate the first n Fibonacci numbers as a list
# (Idea: https://stackoverflow.com/questions/494594)
def fiblst(n):
    a, b = 0, 1
    result = []
    for _ in range(n):
        result += [a]
        a, b = b, a+b
    return result

print('classic as a list and with return')
print(fiblst(10))
# Output [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
print(fiblst(80))

# same but as a generator

def fibgen(n):
	a,b = 0,1
	for _ in range(n):
		yield a
		a, b = b, a+b

print('\nas a generator with yield')
print(fibgen(10))
print(list(fibgen(10)))

print(fibgen(80))
print(list(fibgen(80)))

# if you want to check each value for the generator then use next
# next will ned to have a default value, to avoid StopIteration errors
# fibonacci numbers till 1000
gen = fibgen(1000) 
fib = next(gen)

while fib<10000:
	print(fib)
	fib = next(gen, None)
	if fib is None: 
		print('Generator input was less then while end!')
		break