#####################
#      Sets         #
#####################
# Create an empty set
s = set()

# create a set with 1, 2, 3
s2 = {1, 2, 3}

# create a set witch Hello World! using set()
s3 = set('Hello World!')
# print all sets with logical formatting
print(f'Set s: {s}')
print(f'Set s2: {s2}')
print(f'Set s3: {s3}')

# Add elements to set
s.add(1)
print(f'Set s after adding 1: {s}')

# add more than one element
s.update([2, 3, 4])
print(f'Set s after adding 2, 3, 4: {s}')

# remove element from set
s.remove(2)
print(f'Set s after removing 2: {s}')

# do the same with discard
s.discard(2)
print(f'Set s after discarding 2: {s}')

# clear set
s.clear()
print(f'Set s after clearing: {s}')

# using sets with set theory.
s1 = {1, 2, 3, 4, 5}
s2 = {4, 5, 6, 7, 8}

# joins with s1 and s2 with |
print(f'Set s1 | s2: {s1 | s2}')

# diff with s1 and s2 with -
print(f'Set s1 - s2: {s1 - s2}')

# intersection with s1 and s2 with &
print(f'Set s1 & s2: {s1 & s2}')

# symmetric difference with s1 and s2 with ^
print(f'Set s1 ^ s2: {s1 ^ s2}')