#############################
#      Dictionaries         #
#############################
#  A dictionary is a collection of key-value pairs.
#  A dictionary is a collection which is ordered*, changeable and does not allow duplicates.
#  A dictionary is written with curly brackets, and has keys and values:
#  Example:

my_dict = {
    "name": "John",
    "age": 30,
    "city": "New York",
    "country": "USA",
    "is_married": False,
    "skills": ["Python", "Java", "C++"],
    "address": {
        "street": "123 Main St",
        "city": "New York",
        "state": "NY",
        "zipcode": "10001"
        }
    }

print(my_dict)

# color dict 
colorDict = { }
colorDict['red'] = 0xff0000
colorDict['green'] = 0xff00
colorDict['blue'] = 0xff
colorDict['white'] = 0xffffff

# print all
print(colorDict)

# get one element
print(colorDict['blue'])
print(hex(colorDict['blue']))

# try get value
print('gelb' in colorDict)	
print(colorDict.get('gelb', "Upps"))
print(hex(colorDict.get('gelb', 0)))
print(colorDict.get('blue', 'lol'))
print(hex(colorDict.get('blue', 0)))

# get the length of an dict
print(len(colorDict))

# add new values to dict
colorDict['dark'] = 0x000000
print(colorDict)

# get only the values of the dict
print(colorDict.values())

# get only the keys of the dict
print(colorDict.keys())

# convert in to list or set from values and keys
print(list(colorDict.values()))
print(list(colorDict.keys()))

print(set(colorDict.values()))
print(set(colorDict.keys()))

# working trough dicts
for f in colorDict:
	print(f'Color {f} has the colorcode {hex(colorDict[f])}.')

# getting the not 0 colorcodes with comprehension
print([ color for (color, code) in colorDict.items() if code != 0])

# merge to dicts
a = {'a': 10, 'b': 20}
b = {'b': 30, 'c': 40}
union1 = { **a, **b}
union2 = { **b, **a}
print(union1)
print(union2)

# python3.9^
union3 = a | b
union4 = b | a
print(union3)
print(union4)

# zip method
lst1 = list(range(1,11))
lst2 = list('abcdefghij')

zipped = dict(zip(lst1, lst2))
print(zipped)