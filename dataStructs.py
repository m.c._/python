#####################
#      Lists        #
#####################

from functools import reduce
import locale

#Lists
lst = [1, 2, 3, 'jkdsnfgkmn',"kjdnfkdfgn", 12 ]

# get the 4. element
print(lst[3])
# get element 3. till 4.
print(lst[2:4])
# reverse
print(lst[::-1])
# change 4. element
lst[3]='dump'
print(lst)

# more dimensional
dimList = [[1,3], [5,6]]
print(dimList[1][1])
print(dimList[0][0])

# using range
numList = list(range(10, 101, 10))
print(numList)

# casting strings to lists
strList = list('This shall be a list now!')
print(strList)
#joining list x.join x represents the separator
print(''.join(strList))
print('->'.join(strList))

#list comprehension
print([x*2+1 for x in numList])
print([[x, x*x] for x in numList])
for x in numList:
    print(x, x*x, x**3, sep='\t')

# further list methods
# gets the length of the list
print(len(numList))

# adds an element at the end of the list
numList.extend([110])
print(len(numList))

# adds an element at the given position .insert(pos, value)
numList.insert(5, 55)
print(numList)

# delete list element at given position and print it
print(numList.pop(5))

# remove element from the list
numList.remove(80)
print(numList)

# delete the 3. element from right
numList.pop(-3)
print(numList)

# delete last three elements from list
del numList[-3:]
print(numList)

# add three new elements to the list with += 
numList += [111, 222, 333]
print(numList)

# sort the list
numList.sort()
print(numList)

# reverse the list
numList.reverse()
print(numList)

# count the occurence of a value
print(numList.count(111))
print(numList.count(222))

# get the index of a value
print(numList.index(222))
print(numList.index(333))

# append a value to the list
numList.append(444)
# get min and max value
print(min(numList))
print(max(numList))

# get sum
print(sum(numList))

# build tuple from list
tup = tuple(numList)
print(tup)

# build tuple from list do with zip
tup2 = tuple(zip(numList, numList))
print(tup2)

# clear the list
numList.clear()
print(numList)

# define a function that build the x*x of numbers
def square(n):
    return n**2

numListNew = [1,2,3,4,5,6,7,8,9,10]
print("{} - List count is: {}".format(numListNew, len(numListNew)))
# apply the function to the list
numListNew = list(map(square, numListNew))
print("{} - List count is: {}".format(numListNew, len(numListNew)))

# define a function that build the x*x of numbers with lambda
numListNew = [1,2,3,4,5,6,7,8,9,10]
numListNew = list(map(lambda n: n**2, numListNew))
print("{} - List count is: {}".format(numListNew, len(numListNew)))

# with list comprehension
numListNew = [1,2,3,4,5,6,7,8,9,10]
print("{} - List count is: {}".format([x*x for x in numListNew], len(numListNew)))

# define function that adds to values
def add(a, b):
    return a+b

# fill two different lists with values
numList1 = [1,2,3,4,5]
numList2 = [11,12,13,14,15,16,17,18,19,20]

# apply the function to the list
numList3 = list(map(add, numList1, numList2))
print("{} - List count is: {}".format([x*x for x in numList3], len(numList3)))


# reduce the list to a single value
numList4 = [1,2,3,4,5]
print(reduce(add, numList4))

# reduce list with range 1 - 90 to a single value with lambda
numList4 = list(range(1, 91))
print(reduce(lambda a,b: a+b, numList4))

# filter the list with lambda
numList5 = list(range(1, 91))
print(list(filter(lambda x: x%2==0, numList5)))

# filter the list with comprehension
numList5 = list(range(1, 91))
print([x for x in numList5 if x%2==0])

# define modulo function
def modulo(a):
    if (a%2==0):
        return a

# define a list with range 1 - 90
numList6 = list(range(1, 91))

# filer list with modulo
print(list(filter(modulo, numList6)))

# create a list of 3 with only boolean
boolList = [True, False, True]
print(boolList)

# use any and all with the boolList
# this checks if any of the elements are true
print(any(boolList))
# this checks if all of the elements are true
print(all(boolList))

# create list of numbers with 1, 2, 4 and 8
numList7 = [1,2,4,8]
# use any and all with numList7 and comprehension
print(any([x%2==0 for x in numList7]))
print(all([x%2==0 for x in numList7]))

# create a list that contains hallo world!
strList = list('Hallo, world!')
# use .sort()
strList.sort()
print(strList)

strList = list('Hallo, world!')
# sort the strList
print(sorted(strList))
# sort the strList in reverse
print(sorted(strList, reverse=True))
# use sort with key to use methods like lower()
print(sorted(strList, key=str.lower, reverse=True))

# create list using äöü
strList = list('bälöoÜßauUs')
# set locale for mac
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
# sort the strList with key strxfrm
print(sorted(strList, key=locale.strxfrm))
