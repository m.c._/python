def f(x, y):
    if x < 0 or y < 0:
        raise ValueError("x and y must be non-negative")
    return x + y

f(1, 2)
f(-1, 2)