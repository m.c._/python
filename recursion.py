#################
#	recursion	#
#################

def f(n):
    if n<=1:
        return 1
    else:
        return n * f(n-1)

x = input('Please give me a number: ')

result = f(int(x))

print(result)  # 120

def f(n):
    # Attention uncontrolled recursion
    return 1 + f(n)

# f(2) # this uncommented will give an error since the recursion depth is to high!