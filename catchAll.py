def perform_operations():
    try:
        # Example operation that may raise an exception
        result = 10 / 0  # This will raise a ZeroDivisionError
        print(result)
        
        # Example of accessing an undefined variable
        print(undefined_variable)  # This will raise a NameError
        
        # Example of accessing a non-existent list element
        my_list = [1, 2, 3]
        print(my_list[5])  # This will raise an IndexError
        
    except ZeroDivisionError:
        print("Caught a division by zero error.")
        
    except NameError:
        print("Caught a name error.")
        
    except IndexError:
        print("Caught an index error.")
        
    except Exception as e:
        # This is a catch-all for any other exceptions
        print(f"Caught an unexpected exception: {e}")
        
    finally:
        print("This will always execute, regardless of any exceptions.")
def perform_operations2():
    try:
        # Example operation that may raise an exception
        result = 10 / 0  # This will raise a ZeroDivisionError
        print(result)
    except Exception as e:
        print(f"Caught an exception in division operation: {e}")
    
    try:
        # Example of accessing an undefined variable
        print(undefined_variable)  # This will raise a NameError
    except Exception as e:
        print(f"Caught an exception in undefined variable access: {e}")
    
    try:
        # Example of accessing a non-existent list element
        my_list = [1, 2, 3]
        print(my_list[5])  # This will raise an IndexError
    except Exception as e:
        print(f"Caught an exception in list access: {e}")

    # This will always execute, regardless of any exceptions.
    print("All parts of the function have been attempted.")

def perform_operations_pass():
    try:
        # Example operation that may raise an exception
        result = 10 / 0  # This will raise a ZeroDivisionError
        print(result)
    except ZeroDivisionError as e:
        print(f"Caught a ZeroDivisionError: {e}")
    
    try:
        # Example of accessing an undefined variable
        print(undefined_variable)  # This will raise a NameError
    except NameError as e:
        # Silently ignore the NameError using pass
        pass
    
    try:
        # Example of accessing a non-existent list element
        my_list = [1, 2, 3]
        print(my_list[5])  # This will raise an IndexError
    except IndexError as e:
        print(f"Caught an IndexError: {e}")

    # This will always execute, regardless of any exceptions.
    print("All parts of the function have been attempted. NameError was ignored!")

# Call the function
perform_operations()
perform_operations2()
perform_operations_pass()