s = 'abcdefghijklmnopqrstuvwxyz'
x = '123'

print(s.join(x)) # builds tuple with both strings 
print(s.partition('abcdefghijklmn')) # splits s in to tuple 

print(s[3]) # first 3
print(s[3:6]) # 3 to 6
print(s[:3]) # up to index 3 
print(s[3:]) # from the index 4 on
print(s[-4]) # the index 4 from string end
print(s[-4:]) # all from the index 4 from string end

print(s[::2]) # every second char
print(s[:10:2]) # every second char ending at the index 10
print(s[10::2]) # every second char starting at the index 10
print(s[::-1]) # all in reverse
print(s[::-2]) # every second char in reverse
print(s[:10][::-1]) # the first 10 chars in reverse

print('sub' in s) # checks if 'sub' is in s
print('abc' in s) # checks if 'abc' is in s

print(s.upper())
print(s.partition('e')) # splits s in to tuple at char 'e'
print(s.split('e')) # splits in to a list at 'e' and removes e from list

print(len(s))
print(str.isalpha(s)) # checks if s only contains chars
withWhitespaces = s + ' with whitespaces'
print(str.isalpha(withWhitespaces)) # checks if var only contains chars
print(str.isdigit(x)) # checks if x only contains numbers
numbAndChars = x + s
print(str.isdigit(numbAndChars)) # checks if var only contains numbers
print(str.isalnum(numbAndChars)) # checks if var only contains numbers and chars
numbAndCharsWithSpaces = x + s + " "
print(str.isalnum(numbAndCharsWithSpaces))
print(str.isascii(numbAndCharsWithSpaces)) # checks if var only contains ascci chars
print(str.islower(numbAndCharsWithSpaces))
print(str.isupper(numbAndCharsWithSpaces))