#########################
#          ifs          #
#########################

x = 42
s ='this is a string'

# removing the whitespaces and building a new string
split = ''.join(s.split(' '))
print(split)

# simple if with else
if str(x).isdigit() is False:
	print(f'x isn\'t a number: {x}')
else:
	print(f'x is a number: {x}')

# checks if its null or empty
if x:
	print('x is not null/none')

# checks if its a string with elif
if s.isdigit():
	print('s is a number')
elif s.isalpha():
	print('s is a string')
else:
	print('s is unknown')

# same as above but with the split
if split.isdigit():
	print('split is a number')
elif split.isalpha():
	print('split is a string')
else:
	print('split is unknown')

# ad a pass to the if statement -> pass is a placeholder to avoid errors
if x:
	pass # Todo, add something her later to check the condition above
else:
	print('x is null/none')

# short form if statement
inp = input('Enter a number: ')
s = 'even' if int(inp)%2==0 else 'not even'
print(s)

# no switch no drama
day = int(input('enter a number: '))
if day in (1, 2, 3, 4, 5):
	print('work!')
elif day in (6, 7):
	print('weekend!')
else:
	print('failed!')

# doing it with a dict
day = int(input('enter a number: '))
alldays = {
	1: 'monday',
	2: 'tuesday',
	3: 'wednesday',
	4: 'thursday',
	5: 'friday',
	6: 'saturday',
	7: 'sunday'
}

if day in alldays:
	print(alldays[day])
else:
	print('failed!')

# checking if a given year is a Leap year
# modulo 4 is 0 it's a Leap year
# if modulo 100 is 0 it's not a Leap year
# if modulo 400 is 0 it actually always is a Leap year

year = int(input('Give me a year (format yyyy): '))
if year % 400 == 0:
	leap_year = True
	print(f'{year} is a leap year? {leap_year}')
elif year % 100 == 0:
	leap_year = False
	print(f'{year} is a leap year? {leap_year}')
elif year % 4 == 0:
	leap_year = True
	print(f'{year} is a leap year? {leap_year}')
else:
	leap_year = False
	print(f'{year} is a leap year? {leap_year}')