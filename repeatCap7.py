from functools import reduce
import random

# Create a list with multiples of 7
# straged with range and stride
sevenList1 = list(range(7, 100, 7))
print(sevenList1)

# filter it from a list from 1 to 100
hundred = list(range(1,100))
sevenList2 = list(filter(lambda x: x%7 == 0, hundred))
print(sevenList2)

# starting with a list 1 to 14 and multipling 7 to them
startList = list(range(1,15))
sevenList3 = [x*7 for x in startList]
print(sevenList3)


# find a, e, i, o, u in Hello world!
helloList = list('Hello, World!')
vocals = list(filter(lambda x: x in ('a', 'e', 'i', 'o', 'u'), helloList))
print(vocals)
print(''.join(vocals))
# make it work with lower and upper case
vocals = list(filter(lambda x: x.lower() in ('a', 'e', 'i', 'o', 'u'), helloList))
print(vocals)
print(''.join(vocals))

# lottery numbers sorted and unique -> this is indicating for sets!!!
random_set = set()

while len(random_set) < 6:
    random_number = random.randint(0, 49)
    random_set.add(random_number)

print(random_set)
sortedList = sorted(random_set)
print(sortedList)

# find common chars from to strings
s1 = 'Python'
s2 = 'Programming'

print(set(s1) & set(s2)) # this builds sets from the strings
# sort them
print(sorted(set(s1) & set(s2)))

# remove doubled values from list
doubledList = [1, 2, 3, 2, 7, 3, 9]
result = sorted(set(doubledList)) # sets can not contain doubled values so by casting it into a set we remove doublings
print(result)

# how to build a translater dict for numbers
# this calls for dicts
# but bear in mind that it only works one way!!

words = {
	'one': 'eins',
	'two': 'zwei',
	'three': 'drei',
}

print(words['two'])

# Filter string for words only and count them
s = 'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

# kill all punctuation
plain = ''.join(filter(lambda c: str.isalpha(c) or c ==' ', list(s)))
print(plain)

# building the words
words = plain.split(' ')
print(words)

# sort and get the word with the biggest len
#V1 slow
sortedwords = sorted(words, key=len, reverse=True)
print('Max len: ', len(sortedwords[0]))

#V2 faster with reduce and map
maxlen= reduce(max, map(len, words))
print('Max len: ', len(sortedwords[0]))
