var1 = 2 + 3 * 4 # Mit operanten (2...) und operatoren + der *
3
# Ausdruck = operatnen + operatoren
# Bewertung der Operatoren von links nach rechts = niedriger
#

var1 = None
print(var1)
var1 = "Text"
print(var1)
var1 = None
print(var1)
var2 = True
var3 = False
print(var2)
print(var2 + 1)
var4 = 1
var5 = 1.1e3
print(var4)
print(var5)
var6 = 1 + 4j
var7 = "Text\n"
var8 = 'Text'
print(var6, var7, var8)
var1 = 7
print(type(var1))
var2 = "3";
print(var1 + int(var2))
print(str(var1) + var2)
print(type(float(var1)))

var1 = "Die Antwort ist"
print(var1)
var1 = 42
print(var1)
# Definition eigener Funktionen mit: def + name + () -- name muss unique sein!

def zitate():
    print("Sie wars, sie wars. Er wars, er wars")
    print("Dein Volk ist müde und hat nichts zu Mittag gegessen")
zitate()
# Hauptfunktion ! bei Python nicht zwingend nötig

def main():
    pass # Hauptprozedur des kompletten Programmflusses
main()
# Übergabeparameter an Funtkion

def multi(a, b):
    # a und b stehen in der Funktion als Parameter bereit !
    # werden durch Werte ersetzt beim Aufruf
    erg = int()
    a += 1
    print(a * b)
    erg = a * b
    return erg 

def testbitch(c):
    print(c)

lol = float()

multi(4,5)
var1 = 3    # Veränderung nur in der Funktion
'''
    Grund ist weder call-by-value noch call-by-reference, sonder zwischending call-by-object
    call-by-reference mmit funktion call-by-value
'''
multi(var1, 5)
lol = multi(5,6)
print(var1)
print(lol)
testbitch("What the fuck! Bitch")
def multi(a,b):
    '''
        Eine Funktion, die sich so verhält, nannte man früher Prozedur.
        Denn sie liefert kein Ergebnis, sie ist Selbstzweck, Sie kümmert sich um die Ausgabe und fertig.
    '''
    print(a * b)
    return(a * b) # Sprunganweisung Rückgabewert !
    print("lol") # Wird nie erreicht unreachable code !!!!
    
'''
    Eine Funktion, die sich so verhält, nannte man früher Prozedur.
    Denn sie liefert kein Ergebnis, sie ist Selbstzweck, Sie kümmert sich um die Ausgabe und fertig.
'''
    
a = multi(4,5)
print("Ergebnis war:")
print(a)
# Def immer vor Funktionsaufruf !!!!!
def zitat1():
    print("Ausschlupfhausen bitte sehr!")
    
def zitat2(text):
    print(text)
    
zitat2("Wo kommt ihr den her?")
zitat1()
# zitat2("lol", 1) geht nicht weil zuviele Parameter

# Deklarationen Globale Variablen und Funktionen
a = 42

def ausgabe1():
    a = 1 # so ist a +=1 möglich Lokale-variable die Globale verdeckt
    print(a)
    # a += 1 geht nicht wenn dann so wie unten weil a hier lokal wird und wir auf globale zugegriffen wird !

def ausgabe2():
    global a
    a += 1 # Globaler werd wird geändert !!! nicht gut
    print(a)

def ausgabe3():
    print(a)    
   
ausgabe1()
ausgabe3()
ausgabe2()
print(a)

# Closure

def rechnen(a):
    pot = 3
    def innen():    # innere Funktion
        text = "Das Erg ist:"
        print(text, a ** pot) # ** potenzieren
    innen() # nur in rechnen() verfügbar
rechnen(4)
rechnen(5)
rechnen(7)

'''
    Die Funktion "innen" ist jedoch nicht außerhalb der Funktion zugänglich. Und wie gesagt, dieses gesamte Verfahren nennt man ein Closure. Mittels dieser Closure
    Technologie kann man ein Konzept der Datenkapselung was in der Objekt und hier in der Programmierung von Bedeutung ist, auch funktional umsetzen. Man kann diese
    Technik aber auch bei Methoden in Python benutzen.
'''

# Funktion
'''
    In Python muss die Anzahl der Parameter beim Aufruf, exakt mit der Anzahl der Parameter bei der Deklaration übereinstimmen.
    Es gibt aber zwei Sondersituationen, wo dem nicht so ist. Es gibt die Möglichkeit, sogenannte optionale Parameter festzulegen
    oder eine sogenannte Liste variabler Länge. 
'''
def rechnen(a,b = 8): #optionaler Parameter b, nur verwendet wenn nichts beim Aufruf mit übergeben wird!
    print(a*b)
def multiplizieren(a, *b): # b ist eine Liste variabler Länge (*x), mindestens a muss da sein Rest ist egal wichtig ist logische Programmierung
    '''
        optionale Parameter immer nach dem Festen
    '''  
    erg = a
    for i in b:
        erg *=i
    print(erg)

rechnen(2)
rechnen(3,4)

multiplizieren(3)
multiplizieren(3,5)
multiplizieren(3,5,7,11)
# Kontrolstrukturen
a = 1
if a < 2: # Aufbau der If-Anweisung
    print("Der Wert von a ist kleiner 2") # Einrückung wichtig 4 oder Tab !
    print("Auch das wird bedingt ausgeführt")

a = 3
if a < 2: # (a < 2) muss nicht sein
    print("Der Wert von a ist kleiner 2")
    print("Auch das wird bedingt ausgeführt")
elif a == 3: # else if
    print("Der Wert ist 3")
else: # else
    if(a == 4):
        print("Der Wert ist 4")
# Neue Anweisung !
if a > 2: print("Das geht") # Selbe Ebene geht auch!
else: print("Alternative")

# Schleifen -- es geht nur while 
i = 0 # Zählvariable

# Aufbau Schleife
while (i < 9): 
    i += 1 # i++ geht in python nicht !!
    print("Wert des Schleifenzählers", i)
print("Schleife beendet")

i = 0
while (True): 
    if i == 9: break
    i += 1
    print("Wert des Schleifenzählers", i)
    
print("Schleife beendet")
# For geht nur in diesem Fall
# Geht String Zeichen für Zeichen druch !
zitat = "Werft den Purschen zu Poden"
zv = 0
for x in zitat:
    zv += 1
    print("Zeichen ", zv, ":\t", x)    
print("Schleife beendet")
# Ausbrechen der Schleife
i = 0
while (True):
    if i >= 5: break    
    i += 1
    print("Wert des Schleifenzählers", i)  
print("Schleife beendet")

i = 0
while (i < 10):
    i += 1
    if ((i % 2) != 0): continue # nächster Druchlauf
    print("Wert des Schleifenzählers", i)  
print("Schleife beendet")

'''
    Als weitere Sprunganweisung gibt es noch "raise", was im Zusammenhang mit
    Ausnahmebehandlung von Bedeutung ist und weit über dem Scope von diesem Kurs liegt und
    "return", was den Rückgabewert einer Funktion liefert und was man sich anschaut ,wenn
    man Funktionen betrachtet.
'''
i = 0
while (i < 10):
    i += 1
    if ((i % 2) != 0): continue
    print("Wert des Schleifenzählers", i)
print("Schleife beendet")
#Klassen definieren

class Katze:
    name = "Herbi" #Werte müssen nicht zwingend vergeben werden!
    alter = 6
    def lautgeben(self): #Methoden wie Funktionen einer Klasse, mit def eingeleitet und immer (self)!
        print("Miau")

#Objekte aus einer Klasse erzeugen
obj = Katze() #Katze()=Konstruktor, ohne new wie in Java

print(type(obj)) #gibt Typ wieder

print(obj.name)

obj.lautgeben()
print(obj.alter)

obj.name = "Schnursula"
print(obj.name)
#Klassen: Magische Methoden ( __xx__ )

class Tier:
    def __init__(self, name, alter, typ): #Initialisierung der Datentypen 
        self.typ = typ
        self.name = name
        self.alter = alter

    def lautgeben(self, text):
        print(text)
        
        '''
            Immer bei Methoden/Funktionen auf Übergabeparameter achten!!
            Self wird nicht als Parameter gezählt da er raus fällt.
        '''

obj = Tier("Herby", 6, "Katze")
print(obj.typ)
obj.lautgeben("Miau\n")

obj2 = Tier("Benno", 4, "Hasi")
print(obj2.typ)
obj2.lautgeben("Schnüffel\n")


class Person:
    alter = 42

obj = Person()
print(obj.alter)


class Person2:
    __alter = 42 #private Variable!!!
    '''
        Methode nötig um auf Variable zu zugreifen. Schreibschutz bei default!!
    '''
    def getAlter(self): # get und set sind nötig
        return self.__alter
    
obj2 = Person2()
print(obj2.getAlter())
#Vererbung in Python

class Lebewesen:
    alter = 42

class Mensch(Lebewesen): #Läuft über (class-name)
    name = "Hans"

obj = Mensch()
print(obj.alter, obj.name)

'''
    Die Syntax sehen Sie hier, bei der Klasse wird in runden Klammern die sogenannte Superklasse angegeben.
    Das ist die Klasse von der man erbt. Mensch ist eine Subklasse das ist die Klasse die erbt.
    Die erbende Klasse, also die Subklasse fügt Funktionalität hinzu, entweder Eigenschaften
    oder Methoden oder verändert irgendetwas am Erbe, ansonsten wäre sie ja identisch und das wäre unsinnig.
    Was machen also jetzt machen kann, ist ein Objekt erzeugen vom Typ Mensch und
    über dieses Objekt steht sowohl das Alter, als auch der Name zu Verfügung. Der entscheidende Punkt ist,
    der Name wurde direkt in der Subklasse deklariert. Das Alter allerdings von der Superklasse vererbt,
    mit diesem Konzept der Vererbung lässt sich die Wiederverwendbarkeit sehr effizient umsetzen
    und das ist das Wesen von der objektorientierten Programmierung, dass man versucht ein API
    möglichst objektorientiert über Vererbung aufzubauen. Was immer man schon hat, vererbt man,
    um das Rad nicht jedes Mal wieder neu erfinden zu müssen.
'''
#Startmethode
'''
    In vielen Sprachen ist es üblich, dass ich ein Programm aus einer zentralen Startfunktion oder auch Methode heraus entwickelt.
    So etwas muss man in Python nicht machen, aber man kann es machen und in diesem Video möchte ich Ihnen zeigen,
    wie man einen objektorientierten Startpunkt. Das heißt, eine Klasse mit einer zentralen Methode bauen würde.
    Beispielsweise "init" und aus dieser Methode heraus, entwickelt sich das komplette Programm.
'''
class Root:
    def init(self):
        print("Das Programm wird gestartet")
'''        
    Nun muss diese Methode aufgerufen werden und dazu instanziiert man diese Klassen, das macht man so.
    Normalerweise weißt man dieses Objekt einer Variablen zu, diese Objektvariable referenziert dann das Objekt.
    Aber für den Fall, dass man wirklich dieses Objekt nur einmal braucht und dann auch dort nur einmal eine Methode
    aufruft oder eine Eigenschaft abfragt, kann man so etwas tun. Das ist eine anonyme Erzeugung eines Objektes.
    Dieses Objekt steht zur Verfügung, man ruft die Methode auf und danach ist das Objekt nicht mehr notwendig.
    Diesen Ansatz kann man sehr schön hier bei so einer Wurzeltechnologie, wo sich ein Programm eben aus
    einer Klasse dort einer zentralen Methode entwickelt, durchführen.
'''

Root().init()
# Divison
print(4 / 2)
print(5 / 2)

# Verhindern der gleitkomma zahl
print(4 // 2)
print(5 // 2)

a = 17
b = 0.3
c = 2
d = 0.65
print(1 * 1) # 1
print(1 + 1) # 2
print(5 - 1) # 4
print(4 / 2) # ist 2
print(5 / 2) # ist 2.5 in python !!

# Modolo
print(a % c) # = 1
print(a % b) # 0.20000000000000062 -- Rundungsfehler bei der länge
print(a % d) # 0.09999999999999942 -- Rundungsfehler bei der länge 
# kein Modolo bei Float/Double

# potenzzeichen
print(a ** c) # 289
print(a ** b) # 2.3395626336814512

# + auch stringverkettung
print("a" + "b") # ab
print(a + 1.9) # 18.9
print(1 + 3.9) # 4.9
print(False + True) # 1
print(a + True) # 18


myVar = 5
# Verknüpftezuweisungsoperation
myVar *= 2 # myVar = myVar * 2

print(myVar)
myVar %= 13 # myVar = myVar % 13

# Geht für alle aretmetischen Operatoren
print(myVar)
myVar **= 2
print(myVar)

print ("8 != 4 + 4:",8 != 4 + 4) # False
print ("7 != 4 + 4:",7 != 4 + 4) # True
print ("5 < 9:",5 < 9) # True
print ("9 <= 9:",9 <= 9) # True

print ("4 == 5:",4 == 5) # False
print ("\"4\" == 4:","4" == 4) # False
print ("4 == 2 + 2:",4 == 2 + 2) # True

print ("5 > 9:",5 > 9) # False
print ("5 >= 5:",5 >= 5) # True
print ("9 >= 5:",9 >= 5) # True
print ("4 >= 5:",4 >= 5) # False
print ("\nKomplexer -- Boolsche operatoren !!:\n") 
print("(4 + 4 == 8) and (2 + 3 == 5):",(4 + 4 == 8) and (2 + 3 == 5)) # True
print("(4 + 4 == 8) and (1 + 3 == 3):",(4 + 4 == 8) and (1 + 3 == 3))# False
print("(4 + 4 == 8) or (2 + 3 == 5):",(4 + 4 == 8) or (2 + 3 == 5)) # True
print("(4 + 4 == 7) or (2 + 3 != 5):",(4 + 4 == 7) or (2 + 3 != 5)) # False
print("not(1 + 4 == 5):",not(1 + 4 == 5)) # False

pattern = "@"
seq1 = "ralph_steyer@gmx.de"
print(pattern in seq1 ) # enthalten
print(pattern not in seq1 ) # nicht enthalten
seq2 = "ralph_steyer@gmx.de"
print ("seq1 is seq2", seq1 is seq2) # Gleichheit der Objekte String anders !
print ("seq1 == seq2", seq1 == seq2) # Gleichheit

z1 = [2, 3, 5] # sequenziele Datentypen
z2 = [2, 3, 5] 

print ("z1 is z2", z1 is z2) # hier nicht gleiches Objekte Adressen!
print ("z1 == z2", z1 == z2)


z1 = 2
a = z1 << 1 # Bitverschiebung
b = z1 >> 1 # Bitverschiebung
print("Der Wert von 2 << 1: " , a)
print("Der Wert von 2 >> 1: " , b)
"Hallo Welt"
a = (2 + 3) * 4
a = 5
1
#!/usr/bin/python
# Das ist der Nachname
name = "Meier"
print(name)
vorname = "Hans" # Das ist der Vorname
print(vorname)
'''
  Das ist das Alter und die Dokumentation
  steht in mehreren Zeilen
'''
alter = 42
print(alter)
print(42)
print(name, vorname)
name = "Meier"
vorname = "Hans" 
alter = 42
print(name, vorname, alter, sep="-")
name = input("Bitte den Nachnamen eingeben")
vorname = input("Bitte den Vornamen eingeben")
alter = input("Bitte das Alter eingeben")
print(name, vorname, alter)
z1 = int(input("Bitte Zahl1 eingeben"))
z2 = int(input("Bitte Zahl2 eingeben"))
print(z1 + z2)
print(eval("2 + 3 + 4"))
from tkinter import *
master=Tk()
master.mainloop()
# Sequenzielle Datentypen

# Arrays
var1 = "Oh, ich habs mir überlegt, wir lassen das mit Camelot... der Fernsehempfang ist dort zu schlecht!"
var2 = bytes([0x13, 0x11, 0x10, 0x24, 0x08, 0x01])
var3 = bytes([2,3,5,7])

for x in var3:
    print(x)
print(var3)

print("var2")
for x in var2:
    print(x)
print(var2)
    
print("var1")
for x in var1:
    print(x)
print(var1)

'''
    Zeichenkette wie gesagt, ist einer der Datentypen die sehr, sehr wichtig sind die man aber normalerweise gar nicht als sequenziellen Datentyp betrachtet,
    sondern man benutzt in der Regel diese Zeichenkette, gibt sie ja aus verarbeitet, sie auf vielfältige Art und Weise.
    Aber de facto ist hier ein sequenzieller Datentyp dahinter und die Byte-Arrays, die es zwar gibt, die sind eher nur am Rande interessant.
    Die anderen sequenziellen Datentypen von Python hingegen, sind sogar eines der großen Highlights dieser Sprache.
'''
# Tupel

primzahlen = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31)
print(primzahlen)
'''
    Man nennt so etwas „immutable“. Man macht also bei der Deklaration eines Tupels, runde Klammer auf,
    durch Komma getrennt werden die Einzelwerte hingeschrieben und dann kann man mit diesem Tupel arbeiten.
    Die "print"-Funktion kann das gesamte Tupel auf einmal ausgeben beispielsweise.
'''
wochentage = ("Mo","Di","Mi","Do", "Fr", "Sa", "So")
print(wochentage)
matrix = ((1, 2), (3, 4)) # Tupel in Tupel ist eine Matrize
'''
    "matrix", weil das ist im Grunde so etwas wie eine mathematische Matrix, die ich hier abbilde.
    Wir schauen uns das mal an, was diese "print"-Funktion damit macht. Sie sehen im Wesentlichen die einzelnen Werte
    auf eine gewisse Art und Weise von der "print"-Funktion aufbereitet.
'''
print(matrix)
tupel1 = (primzahlen,wochentage)
print(tupel1)

#in 
'''
    Der Membership Operator "in" ist elementar in sequenziellen Datentypen.
    Man kann ihn auf Texte anwenden, aber auch auf Tupel, auf verschachtelte Tupel und andere 
    sequenzieller Datentypen, so wie sie in Python vorhanden sind
'''
text = "Spam. Wonderful spam."
primzahlen = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31)
wochentage = ("Mo","Di","Mi","Do", "Fr", "Sa", "So")
matrix = ((1, 2), (3, 4))

print("o" in text)
print(13 in primzahlen)
print(14 in primzahlen)
print(1 in matrix)          #False weil Elemente selbst Tupel und nicht zahl 
print("o" in wochentage)    #False weil Texte sind sequenzielle Datentypen

'''
    Das, das und das hier, wie erwartet, aber warum wird die 1 nicht hier in diesem Tupelmatrix gefunden,
    in diesem verschachtelten Tupel? Und warum wird der Buchstabe "o" nicht in Wochentage gefunden?
    Die 1 wird deswegen nicht gefunden, weil die Elemente, die ich hier durchsuche, selbst wieder Tupel
    sind. Ich suche aber nach der Zahl. Ich kann also hier nur nach dem Tupel suchen und
    der Buchstabe "o" kommt auch nicht vor, weil diese Texte hier, ja selbst wieder
    sequenzieller Datentypen sind. Und ich müsste hier, sozusagen in diesen Montag oder in diesem Mittwoch,
    diese Abkürzung hier suchen, nach dem Buchstaben. Nur dann würde ich "True" liefern. Das heißt also,
    das Verwenden von diesem Membership Operator ist im Grunde sehr einfach aber es gibt einige Feinheiten zu beachten.
    Sie müssen genau wissen wo Sie suchen und was Sie suchen.
'''
# Einzelwerte ansprechen
'''
    es ist eher unüblich, dass man so einen Datentyp wie hier, das da oder auch das da und
    schon gar dieses verschachtelte Tupel hier, einfach mit einem "print" ausgibt.
    Man möchte in der Regel eine einzige Stelle, ein Element in diesem sequenziellen Datentyp ansprechen
    und dann verarbeiten, ausgeben oder sonst was mitmachen. Eine Möglichkeit einzelne Elemente zu bekommen,
    ist der Iterator "for x in“, aber dann bekommen Sie ja immer alle und Sie wollen manchmal ja nur
    eins oder zwei von diesen Elementen haben. Diese sequenziellen Datentypen in Python sind indiziert.
    Und das heißt, es gibt grundsätzlich, wenn man in eckigen Klammern einen numerischen Index
    hinschreibt die Möglichkeit, auch genau ein Element in diesem sequenziellen Datentyp anzusprechen.
'''

text = "Spam. Wonderful spam."
primzahlen = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31)
wochentage = ("Mo","Di","Mi","Do", "Fr", "Sa", "So")
matrix = ((1, 2), (3, 4))

# ähnlich wie bei Arrays

print(text[2])          #Erstes Element immer 0
print(text[0])
print(primzahlen[5])
print(primzahlen[9])

print(matrix[0][0])
print(matrix[0][1])

print(wochentage[0][1])
print(wochentage[5][0])
'''
    Auch Matrizen so kann man die einzelnen Buchstabe besser ansprechen.
'''
print(wochentage[5][1])
'''
    Auch "Di" ist ja ein Text und damit wieder in sequenzieller Datentyp und
    da können Sie genauso wieder indizieren, das haben wir ja schon gesehen hier.
    Und das geht auch bei sozusagen verschachtelten Strukturen, denn Wochentag, ist wenn man genau hinschaut,
    auch ein verschachteltes Tupel, weil die Elemente selbst darin ja selbst wieder sequentielle Datentypen sind.
    Auch wenn es Strings sind, die in gewisser Weise so eine Sonderposition, in den sequentiellen Datentypen übernehmen.
    Halten wir fest, Sie können in jedem sequenziellen Datentyp, die einzelnen Elemente ansprechen,
    indem Sie den Index benutzen, der automatisch vorhanden ist. Und in eckigen Klammern schreiben Sie dann 0 indiziert,
    das entsprechende Element hin, was Sie benutzen wollen.
'''
#Namen
'''
    Über den Namen eines sequenziellen Datentyps und einen Index können Sie gezielt ein Element,
    in dem sequenziellen Datentyp ansprechen. Sie können aber auch mehrere Elemente ansprechen,
    indem Sie sogenannte Index-Bereiche angeben und das wollen uns jetzt mal anschauen.
'''
text = "Spam. Wonderful spam."
primzahlen = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31)
matrix = ((1, 2), (3, 4))

print(text[2:8]) # 3. bis 9. Position aber 9 ist nicht eingeschlossen!
print(primzahlen)
print(primzahlen[3:len(primzahlen)]) #len ist die Länge des Objekts
print(primzahlen[5:len(primzahlen)-1])


# Dynamische Listen/variable Arrays
# Tupel
primzahlen = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31)
print(primzahlen)

'''
primzahlen[2] = 111
    Das geht bei Tupel nicht !!!!
'''

wochentage = ("Mo","Di","Mi","Do", "Fr", "Sa", "So")
print(wochentage)
matrix = ((1, 2), (3, 4))
print(matrix)
tupel1 = (primzahlen,wochentage)
print(tupel1)

'''
    Unterschied ist, Listen sind dynamische man kann in ihnen Werte ändern
'''

# Listen [] indiziert eine Listen
primzahlen = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]
print(primzahlen)
primzahlen[2] = 111 #Funktioniert schon 
print(primzahlen)
primzahlen.append(39) #Erweitern der Liste !!! wird ans Ende angefügt 
print(primzahlen)

primzahlen.pop(len(primzahlen)-1) #Element an einer stelle Entfernen hier letztes ! -1 wegen in 0 indiziert
print(primzahlen)
primzahlen.pop(len(primzahlen)-5)
print(primzahlen)
wochentage = ["Mo","Di","Mi","Do", "Fr", "Sa", "So"]
print(wochentage)
matrix = [[1, 2], [3, 4]]
print(matrix)
matrix.insert(2, [5,6]) # insert(pos, val)
print(matrix)
matrix.remove([3, 4]) # remove(vale)
print(matrix)
matrix.reverse
liste1 = [primzahlen,wochentage]
print(liste1)
liste1.reverse()
print(liste1)

'''
    Mit "insert" können Sie ein Element hinzufügen, mit "remove" können Sie ein Element aus der Liste entfernen,
    das mit einem bestimmten Wert übereinstimmt und so weiter und so weiter. "reverse" beispielsweise dreht die Elemente
    in einer Liste um. Sie sehen also, Listen sind voll dynamisch und können bezüglich der Elemente geändert werden.
    Sowohl was die Werte, als auch die Anzahl und Positionen der Elemente betrifft. Ansonsten lassen sich vollkommen
    identisch wie Tupel verwenden.
'''


# Dictionaries/Assoziertes Array

person = ["Hans", "Otto"]
print(person[0]) #Zugriff auf Liste

'''
    Problem der Lesbarkeit
'''

person2 = { "vorname" : "Otto", "nachname" : "Hans"}

'''
    Es gibt die Möglichkeit in Python, sogenannte Dictionaries oder in vielen anderen Sprachen auch als Hash-Map
    oder Hash-Struktur oder auch als assoziierte Array bekannt, anzulegen. Man schreibt diese in geschweifte Klammern
    und der entscheidende Punkt ist, dass man sprechende Indizes verwendet. Die Leerzeichen dazwischen
    sind nicht notwendig, aber sie machen die Sache etwas übersichtlicher. So eine Struktur die als Jason
    mittlerweile auch sehr populär ist, wenn man an JavaScript denkt. Diese Struktur ist eben über
    die assoziierten Schlüssel, also Key Value System immer viel, viel sprechender und man kann jetzt
    gezielt in eckigen Klammern diesen sprechenden Index angeben, also beispielsweise "vorname".
    Oder eben auch "nachname". Nun möchte man zum Beispiel über diese Struktur auch iterieren. 
    Und da bietet sich der "for"-Iterator an. Nur ist es normalerweise jetzt nicht sinnvoll,
    direkt über diese Struktur zu iterieren, das kann man zwar auch machen, aber Sie sehen an der Ausgabe,
    dass dann die Keys geliefert werden und wir wollen ja normalerweise die Values haben oder beides zusammen.
'''
print(person2)

print(person2["vorname"])
print("Nur Keys:")
for v in person2:
    print(v)                # gibt nur Keys
for v in person2.values(): 
    print(v)                # gibt Werte
for v in person2.keys():
    print(v)                # wieder nur keys
'''
    So ein Dictionary hat auch jetzt bestimmte Methoden. Manche Methoden sind für alle
    sequenziellen Datentypen gleich, einige sind sehr speziell und hier kann ich zum Beispiel
    eine Methode "update" benutzen, um einen vorhandenen Wert zu aktualisieren. Also beispielsweise
    in geschweifte Klammern "vorname" Doppelpunkt, "Willi" und danach geben wir erneut dieses Dictionary einmal aus.
    Und Sie sehen, dass der Vorname hier aktualisiert ist. Weitere spezielle Methoden wären beispielsweise "get",
    um mit dem Schlüssel den Parameter zu bekommen oder mit "pop" kann man einen Wert entfernen,
    mit "del" kann man einen Wert über seinen Schlüssel löschen und so weiter.
    Dictionaries bieten sich als immer dann an, wenn man einen sprechenden Index braucht und über
    einen sprechenden Index der Zugriff deutlich ist.
'''
person2.update({"vorname":"Willi"})
for v in person2.values():
    print(v)
x = person2.get("nachname", 'notfound')
print(x)
person2.pop("nachname")
for v in person2.values():
    print(v)
del person2["vorname"]
print(person2)

#Mengen 

'''
    Es gibt in Python auch ungeordnete Mengen. Das heißt, da werden einfach
    zusammengehörige Informationen in einer gemeinsamen Struktur verwaltet,
    ohne dass die Reihenfolge eine Rolle spielt man nennt so etwas auch einen Set.
'''

menge1 = set()
print(menge1)

print("Zweite Menge:")
menge2 = set("Graz")
print(menge2)
#add um Menge zu erweitern Menge = BLOB
menge1.add(1)
print(menge1)
#pop liefert erstes Element der Menge und löscht es
print(menge2.pop())
print(menge2)

#clear leert Menge
menge2.clear()
print(menge2)

#Kurznotation wie Liste aber ohne Keys!
print("Menge 3:")
menge3 = {"Graz","Frankfurt"}
print(menge3)

#varibale Typisierung!
'''
    schlechter Stil
'''
var1 = "Antwort ist:"
print(var1)
var1 = 42
print(var1)

'''
    ausnahme expliziete zuordung verhindert ändern wie oben!
'''
# nicht zugewisen
var1 = None
print(var1)
var1 = "Hallo"
print(var1)
var1 = None
print(var1)

#Boolwerte
var2 = True # => 1
var3 = False # => 0

#Vorsicht!
print(var2)
print(var2 + 1)

#zahlen
var4 = 1
#var4 = 1 #Long int
var5 = 1.0 #float
print(var4)
print(var5)

#imaginäre Zahlen
var6 = 1 + 4j
print(var6)

#Chars
char1 = "Text\n"
char2 = 'Text'
print(char1, "\n",char2)
var1 = 7
#typebestimmung
'''
    Diese heißt "type" und für diesen Fall hier, bekommen wir dann halt ein "int". Beachten
    Sie, dass diese Notation "class" relevant ist. Im Hintergrund ist Python
    objektorientiert und arbeitet mit sogenannten Referenztypen, auch wenn es im
    Vordergrund nicht so scheint. Man geht eigentlich hier von aus, dass das ein
    sogenannter primitiver Datentyp ist und vom Schreiben des Python-Codes geht man auch
    quasi mit primitiven Datentypen heran, aber das System im Hintergrund macht daraus
    einen sogenannte Referenztypen.
'''
print(type(var1))
var2 = "3"
#erkennt unterschiedliche datentypen!!
#print(var1 + var2)
'''
    lösung ist ein cast aber muss möglich sein!
'''
print(var1 + int(var2)) #int cast
print(str(var1) + var2) #str cast

 
print(type(float(var1))) #float cast
print (42)
print (11)
print ("Hallo")
print (45);
print (58)
a = 1
print(a)
#variablen deklaration
name = "Maier"
vorname = "Peter" #vorname
alter = 42

#ausgabe
print(name)
print(vorname)
print(alter)
print(name,vorname,alter)
#variablen deklaration
name = "Maier"
vorname = "Peter" #vorname
alter = 42

#ausgabe
print(name,vorname,alter, sep="/")
#variablen deklaration
name = "Maier"
vorname = "Peter" #vorname
alter = 42

#ausgabe
print(name,vorname,alter, sep="/")
#print("wie ist dein name?")
name2 = input("Wie ist dein Name: ")
print("Hallo: ",name2)
z3 = int()
z4 = int()

'''
    Cast in int von String
'''
z1 = int(input("Bitte Zahl eingeben: "))
z2 = int(input("Bitte Zahl eingeben: "))
print(z1 + z2)

z3 = input("Bitte Zahl eingeben: ")
z4 = input("Bitte Zahl eingeben: ")
print(z3 + z4)
#auswertung von String
print(eval("2 + 3 + 4"))
#import von Modulen!

from tkinter import *
master=Tk()
master.mainloop()

#mehr auf python.org
#Variablen richtig verwenden
'''
    _a geht aber besondere Bedeutung (später oder google)
'''
a = 4
muen = 7 #keine Umlaute !!! aber in python3 erlaubt!
btnOK = 1 #verkürzte vorm von buttonOK
'''
    case sensitiv!!
'''
meineVar = "behindert" #Camelnotation! Höckernotation!

a = 4
muen = 7
btnOK = 1
meineVar = 1
