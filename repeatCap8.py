# checking if a given year is a Leap year
# modulo 4 is 0 it's a Leap year
# if modulo 100 is 0 it's not a Leap year
# if modulo 400 is 0 it actually always is a Leap year

year = int(input('Give me a year (format yyyy): '))
if year % 400 == 0:
	leap_year = True
	print(f'{year} is a leap year? {leap_year}')
elif year % 100 == 0:
	leap_year = False
	print(f'{year} is a leap year? {leap_year}')
elif year % 4 == 0:
	leap_year = True
	print(f'{year} is a leap year? {leap_year}')
else:
	leap_year = False
	print(f'{year} is a leap year? {leap_year}')
# append the code above to check to check for month and give the days of the month
month = int(input('Give me a month (format mm): '))
list31Days = [1, 3, 5, 7, 8, 10, 12]
list30Days = [4, 6, 9, 11]
if month == 2:
	if leap_year:
		dayCount = 29
	else:
		dayCount = 28
elif [x for x in list30Days if x == month]:
	dayCount = 30
else:
	dayCount = 31

print(f'There are {dayCount} days in {month} of {year}.')

# calculate the faculty of a given number
faculty = 1
number = int(input('Give me a number: '))
# range in cludes the first number but not the last so we need to add 1
for x in range(1, number + 1):
    faculty = faculty * x

print(f'{x}! = {faculty}')

# sum the result of 1/x*x from 2 to 30
increment = range(2, 31)
sum = 0
for x in increment:
	sum += 1/x*x

print(f'{x}! = {sum}')

# how does range work?
# range(start, stop, step)
# start is the first number
# stop is the last number
# step is the number of steps between each number
# for example range(1, 10, 2) will print 1, 3, 5, 7, 9
# what happens with range(3)
# range(3) will print 0, 1, 2
# what happens with range(1)
# range(1) will print 0
# what happens with range(0)
# range(0) will print nothing
# what happens with range(2)
# range(2) will print 0, 1
for i in range(1,3):
	for j in range(i):
		print(i+j)

# while loop that counts from 100 to 0 with step 5
x = 100
while x >= 0:
	print(x)
	x -= 5
# give all number between 125 and 160 the loop count must be 11
x = 125
y = 160
forCounter = 11
delta = (y - x) / (forCounter - 1)
for i in range(forCounter):
	diff = x + delta * i
	print(diff)

# fill a list with 50 elements with random numbers from 0 to 10
import random
list50 = []
for _ in range(50): # counter is not used in for! -> _
	list50.append(random.randint(0, 10))

# do the same with list comprehension
list50 = [random.randint(0, 10) for _ in range(50)]

# get sum of all elements in the list
sumList = 0
for x in list50:
	sumList += x

print(f'The sum of all elements in the list is {sumList}')

# count the amount of 0 in the list
countZero = 0
for x in list50:
	if x == 0:
		countZero += 1
print(f'The amount of 0 in the list is {countZero}')

# do it more efficiently with count()
countZero = list50.count(0)
print(f'The amount of 0 in the list is {countZero}')

# get the index of the first element with value 0
indexZero = list50.index(0)
print(f'The index of the first element with value 0 is {indexZero}')

# fo it with for loop
for x in list50:
	if x == 0:
		indexZero = list50.index(x)

print(f'The index of the first element with value 0 is {indexZero}')

# what is the difference between list comprehension and generator expression?

# List comprehension and generator expression are both powerful features in Python that allow you to create new lists 
# or sequences in a concise and expressive way. While they may seem similar, 
# there are fundamental differences in their behavior and memory usage:

# 1. **List Comprehension:**
#    - List comprehension is a concise way to create new lists based on existing lists (or other iterable objects) 
# 		using a single line of code.
#    - It returns a new list containing the result of evaluating an expression for each item in the original iterable.
#    - List comprehensions eagerly generate the entire list and store it in memory, 
# 		so they consume more memory when working with large datasets.
#    - They are enclosed in square brackets `[]`.
#    - Example:
#      ```python
#      squares = [x ** 2 for x in range(1, 6)]
#      # Output: [1, 4, 9, 16, 25]
#      ```

# 2. **Generator Expression:**
#    - Generator expression is similar to list comprehension but returns an iterator instead of a list.
#    - It uses parentheses `()` instead of square brackets `[]`.
#    - Generator expressions are more memory-efficient as they produce values on-the-fly, one at a time,
# 		and do not store the entire sequence in memory.
#    - They are particularly useful when dealing with large datasets or infinite sequences because they allow lazy evaluation.
#    - Example:
#      ```python
#      squares_generator = (x ** 2 for x in range(1, 6))
#      # Output: <generator object <genexpr> at 0x7f97f7d3b5c8>
#      ```

# 3. **Lists:**
#    - Lists, on the other hand, are the most common data structure in Python 
# 		that allows you to store and manipulate collections of items.
#    - They eagerly generate the entire list and store it in memory when created.
#    - Lists are mutable, meaning you can modify their elements after creation.
#    - They support various methods for adding, removing, and modifying elements.
#    - Lists can be created using list comprehensions, generator expressions, or by using square brackets `[]`.
#    - Example:
#      ```python
#      my_list = [1, 2, 3, 4, 5]
#      ```

# In summary, list comprehension is ideal when you want to create a new list and use 
# it later in your code, while generator expressions are more suitable when dealing 
# with large datasets or infinite sequences, and you want to generate values on-the-fly 
# without loading the entire sequence into memory. 
# Lists, being mutable and eagerly evaluated, are suitable for scenarios when you need to modify elements 
# or store the entire sequence in memory.

# give samples for list comprehension and generator expression
# list comprehension
listComp = [x for x in range(10)]
print(listComp)

# generator expression
genExp = (x for x in range(10))
print(genExp)