#####################
#      Tuple        #
#####################
from random import randint

# create a tuple with 23, 3, 78
my_tuple = (23, 3, 78)
print(my_tuple)

# same tuple without braces
my_tuple = 23, 3, 78
print(my_tuple)

# print the first element of the tuple
print(my_tuple[0])
# print the last element of the tuple
print(my_tuple[-1])
# print the second element of the tuple
print(my_tuple[1])
# print the third element of the tuple
print(my_tuple[2])
# print the second and third element of the tuple
print(my_tuple[1:3])

# print the length of the tuple
print(len(my_tuple))

# create random number between 1 and 20
random_number = randint(1, 20)
print(random_number)

# check if the random number is in the tuple
print(random_number in (1, 2, 3, 4, 7, 8, 9, 10, 14, 15, 16, 20))

# create a tuple for darkblue with rgb values
dark_blue = (0.1, 0.0, 0.3)
print(dark_blue)

#create a tuple with the messturmente of a rectangular
#room of dimensions 12.5 by 7.5 meters
rectangular_room = (12.5, 7.5)
print(rectangular_room)

# create a tuple with the coordinates of a point with x, y, z
point = (1, 2, 3)
print(point)

# fill a tuple a, b, c with 1, 2, 3
(a, b, c) = 1, 2, 3
# compare the tuple with point
print((a, b, c) == point)

# compare point if it is greater than (2, 3, 4)
print(point > (2, 3, 4))

# compare point if it is less than (2, 3, 4)
print(point < (2, 3, 4))

#  compare point if it is greater than or equal to (2, 3)
print(point >= (2, 2))

# compare point if it is less than or equal to (2, 3)
print(point <= (2, 3))

# fill a with 7 and b with 90 then exchange the values
a, b = 7, 90
print(a, b)
a, b = b, a
print(a, b)