#! /opt/homebrew/Caskroom/miniconda/base/bin/python
#####################
#   pwd generator   #
#####################

from random import randint
from random import choices
from random import shuffle
import string

def pwd_generator(lngth):
    if lngth < 8:
        n=8
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    digits = string.digits
    other = '.,;:-_!$%&()'
    pwlist = choices(lower, k=lngth-3)
    pwlist += [upper[randint(0, len(upper)-1)]]
    pwlist += [digits[randint(0, len(digits)-1)]]
    pwlist += [other[randint(0, len(other)-1)]]
    shuffle(pwlist)
    return ''.join(pwlist)

# test
for i in range(10):
    print(pwd_generator(randint(8, 20)))