# reading from file, without assignment expression
with open('test.txt', 'rt') as txtfile:
    line = txtfile.readline()
    while line:
        print(line, end='')
        line = txtfile.readline() # reads next line

# reading from file, with assignment expressions requires python 3.8 <
with open('test.txt', 'rt') as textFile:
    while line := textFile.readline():
        print(line, end = '')