#########################
#         fors          #
#########################

# For loops
# the loop var can be used later in the source code!
# This is different to some other programming languages

for i in [7, 12, 3, 9]:
	print(i)
print(i)

# for loop over ranges
for j in range(0, 10):
	print(j, end=' ')

# short form of range
for k in range(11):
	if k == 0:
		print(f'\n{k}', end=' - ')
	else:
		print(k, end=' - ')

# for with range and step
for l in range(0, 20, 3):
	if l == 0:
		print(f'\n{l}', end=' - ')
	else:
		print(l, end=' - ')

for m in range(100, 0, -10):
	if m == 0:
		print(f'\n{m}', end= ' - ')
	else:
		print(m, end=' - ')
print('\n')

# since python3 range is a so call generator
# this means the defined values are generated at runtime
# also float ranges can not be generated a work around looks like this:
for n in range(11):
	x = n/10.0
	print(x,end=' ')
print('\n')

# for over strings
for c in 'lolrofl':
	print(c)

# some times it is better to cast a string in to a list
lst = list('lolrofel')
for lt in lst:
	print(lt)

# for and lists, tuples and sets
for t in (17, 87, 4):
	print(t)

for s in ['Python', 'makes', 'fun']:
	print(s)

# getting a counter for loops using enumerate

# defaut counting up
lst = ['Python', 'makes', 'fun']
cnt = 0
for itm in lst:
	print(itm)
	cnt += 1
print(cnt)

# better
for cnt, itm in enumerate(lst):
	print(cnt, itm)

# for with comprehension
lst = [1, 2, 3, 10]
print([ x*x for x in lst])

# only take even numbers
print([ x*x for x in lst if x%2==0 ])

# also possible
print([ [x, x*x] for x in lst ])

# building dicts
dct = { x:x*x for x in lst }
print(dct)

# for and dicts
resultset= { x for x in dct} # this gifs the keys
print(resultset)

resultlist= [ x for x in dct]
print(resultlist)

print({ k for k,v in dct.items()})
print({ v for k,v in dct.items()})

# buliding dicts again
print({ k: v*2 for k,v in dct.items()})

# Generator Expressions
# short form for list comprehension
print(sum([x*x for x in range(100)]))

# more efficient
print(sum(x*x for x in range(100)))

# even join strings
print(''.join(chr(i) for i in range(65,91)))

# using '_' as loop iterator
for _ in range(10):
	print('Hello')

# same with comprehension
[print('Hello') for _ in range(10)]
# do it with .join
print('-'.join('Hello' for _ in range(10)))

# same with generator expression
(print('Hello') for _ in range(10))

# do it with .join
print('-'.join('Hello' for _ in range(10)))