#########################
#        whiles         #
#########################

# this is a simple while
i = 0
while i < 10:
    print(i)
    i += 1

# while over i*i <100000
i = 1
while i < 100000:
    print(i, end=' ')
    i += i*i