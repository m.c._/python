#! /opt/homebrew/Caskroom/miniconda/base/bin/python
########################
#   global and local   #
########################

# given a sample for what is global and what is local
# global variables are accessible from anywhere
# here an example
z = "global"

# a function that uses global variables
def f():
    print(z)
    x = "local" # local variables are only accessible from the function
    print(x)

# call the function
f()
print(z)