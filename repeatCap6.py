# samples for working with dates

from datetime import datetime, time, timedelta
import locale

now = datetime.now()
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
print(now.strftime('%A, %d.%m.'))

s = now.strftime('%A, %d.%m.')
print(s.replace('.0', '.'))

start = time(19, 30)
startToday = datetime.combine(datetime.today(), start)
length = timedelta(minutes=132)
end = startToday + length
print(end.time())
print(end)