from typing import List

class Figure:
    col: int
    row: int

    # Constructor         
    def __init__(self, pos: str):
        if len(pos) != 2:
            raise ValueError("Invalid position")
        c = pos[0].lower() # a-h
        r = pos[1] # 1-8
        if (not c in 'abcdefgh') or (not r in '12345678'):
            raise ValueError("Invalid position")
        self.col = 'abcdefgh'.find(c)
        self.row = '12345678'.find(r)

    # Return position as string
    def __str__ (self):
        return Figure.position(self.col, self.row)
    
    # test if the figure can move to the given position
    @staticmethod
    def position(col: int, row: int) -> str:
        if row < 0 or row > 7 or col < 0 or col > 7:
            return ''
        return 'abcdefgh'[col] + '12345678'[row]
    
class Knight(Figure):
    # Constructor
    def __init__(self, pos: str):
        super().__init__(pos)

    # Return a string representation of the position
    def __str__(self):
        return "Knight at " + Figure.position(self.col, self.row)
    
    # Return a list of possible moves
    def findMoves(self) -> List[str]:
        offsets = [(1,2), (2, 1), (-1, 2), (-2, 1),
                   (1,-2), (2, -1), (-1, -2), (-2, -1)]
        positions = []
        for (coff,poff) in offsets:
            # call static method position
            newpos = Figure.position(self.col+coff, self.row+poff)

            if newpos: # ignore invalid positions
                positions += [newpos]
        return positions

class Bishop(Figure):
    # Constructor
    def __init__(self, pos: str):
        super().__init__(pos)

    # Return a string representation of the position
    def __str__(self):
        return "Bishop at " + Figure.position(self.col, self.row)

    # Return a list of possible moves
    def findMoves(self) -> List[str]:
        positions = []
        for coff in range(-7, 8):
            if coff == 0:
                continue
            # call static method position, diagonal moves from left bottom to right top
            newpos = Figure.position(self.col+coff, self.row+coff)
            if newpos: # ignore invalid positions
                positions += [newpos]
            # call static method position, diagonal moves from right bottom to left top
            newpos = Figure.position(self.col+coff, self.row-coff)
            if newpos: # ignore invalid positions
                positions += [newpos]

        return positions
                   
# Test the classes
knight = Knight("d4")
print(knight)
print(knight.findMoves())

bishop = Bishop("d4")
print(bishop)
print(bishop.findMoves())

# Output:
# Knight at d4
# ['b5', 'c6', 'f5', 'e6', 'b3', 'c2', 'f3', 'e2']
# Bishop at d4
# ['a1', 'b2', 'c3', 'e5', 'f6', 'g7', 'h8', 'a7', 'b6', 'c5', 'e3', 'f2', 'g1']

