# Import the math module with an alias "m"
import math as m

# Now you can use the math module functions with the alias
print(m.pi)  # prints the value of pi

# You can also use the math module functions without the alias
print(m.sin(1))  # prints the sine of 1

# Or, you can use the alias to import specific functions
from math import sin as msin
print(m.asinsin(1))  # prints the sine of 1