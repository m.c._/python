class Player():

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def moveleft(self):
        if self.x > 0:
            self.x -= 1
            return True
        else:
            return False
        
    def moveright(self):
        if self.x < 0:
            self.x += 1
            return True
        else:
            return False
    
    def moveup(self):
        if self.y > 0:
            self.y -= 1
            return True
        else:
            return False
    
    def movedown(self):
        if self.y < 0:
            self.y += 1
            return True
        else:
            return False