class Rectangle:
    # Constructor
    # __init__ is a special method in Python classes, called a constructor.
    def __init__(self, width, height):
        # self is a reference to the instance of the class.
        # It is used to access variables that belong to the class
        # and to call methods of the class.
        # The width and height variables are instance variables.
        self.width: float = width
        self.height: float = height

        # add private variables
        if width < 0 or height < 0:
            raise ValueError('width or height must be positive')
        self.__width = width
        self.__height = height

    # Properties and Setters
    # Properties are used to access and set instance variables.
    # They are defined using the @property decorator.
    # First define a property using the @property decorator.
    @property
    def w(self):
        return self.__width
    
    @property
    def h(self):
        return self.__height

    # Then define a setter method for the property using the @propertyname.setter decorator.
    # Only define a setter method if you want to be able to set the property.
    # If you only want to be able to get the property, you don't need a setter method.
    @w.setter
    def w(self, width):
        if width < 0:
            raise ValueError('width must be positive')
        self.__width = width
    
    @h.setter
    def h(self, height):
        if height < 0:
            raise ValueError('height must be positive')
        self.__height = height

    # Methods
    # area and perimeter are methods of the Rectangle class.
    # They are functions that belong to the class.
    # They can be called on instances of the class.
    # Therefor they are called instance methods.

    # The area method returns the area of the rectangle.
    def area(self):
        return self.width * self.height

    # The perimeter method returns the perimeter of the rectangle.
    def perimeter(self):
        return 2 * (self.width + self.height)
    
    # Using methods in the class definition
    # You can also call methods inside the class definition.
    # This is useful for defining class variables.
    # For example, you can define a class variable that is calculated from other class variables.
    def ap(self):
        return {'area': self.area(), 
                'perimeter': self.perimeter()} # {'area': 200, 'perimeter': 60} for r1 e. g.        

    # Operator Overloading
    # You can define special methods in a class to overload
    # operators like +, -, *, /, ==, <, >, etc.
    # These special methods are called magic methods.
    # For example, you can define a __add__ method to overload the + operator.
    def __eq__(self, other):
        # compare the width and height of two rectangles
        # therefor it creates two tuples with the width and height of the rectangles
        # and compares them
        return (self.width, self.height) == (other.width, other.height)
    
    def __str__(self):
        return f'Rectangle with width {self.width:.2f} and height {self.height:.2f}'
    
    def __repr__(self):
        return f'Rectangle({self.width:.2f}, {self.height:.2f})'

    def __lt__(self, other):
        return self.area() < other.area()
    
    def __le__(self, other):
        return self.area() <= other.area()
    
    def __hash__(self):
        return hash((self.width, self.height))

    # Static Methods
    # Static methods are methods that belong to the class, not to instances of the class.
    # They are defined using the @staticmethod decorator.
    # They do not have access to instance variables, only to class variables.
    # They are called on the class, not on instances of the class.
    # Therefor they are called class methods.
    @staticmethod
    def sarea(width, height):
        return width * height
    
    @staticmethod
    def sperimeter(width, height):
        return 2 * (width + height)
    
    # Using static methods in the class definition
    # You can also call static methods inside the class definition.
    # This is useful for defining class variables.
    # For example, you can define a class variable that is calculated from other class variables.
    # Always use the class name to call static methods. also inside the class definition.
    @staticmethod
    def sap(width, height):
        return {'area': Rectangle.sarea(width, height), 
                'perimeter': Rectangle.sperimeter(width, height)}
    
# Creating instances of the Rectangle class
# r1 and r2 are instances of the Rectangle class.
r1 = Rectangle(10, 20)
r2 = Rectangle(100, 200)

# Calling instance methods
print(r1.area()) # 200
print(r2.area()) # 20000

for r in [r1, r2]:
    print('width', r.width) # 10, 100
    print('height', r.height) # 20, 200
    print('area', r.area()) # 200, 20000
    print('perimeter', r.perimeter()) # 60, 600

# Calling static methods
print(Rectangle.sarea(10, 20)) # 200
print(Rectangle.sperimeter(10, 20)) # 60

# Using static methods in the class definition
print(Rectangle.sap(10, 20)) # {'area': 200, 'perimeter': 60}
print(r1.ap()) # {'area': 200, 'perimeter': 60}
print(r2.ap()) # {'area': 20000, 'perimeter': 600}

# Getter and setter methods in action
# Uses the properties w and h to access the private variables __width and __height.
# The getter method w() returns the value of __width.
try:
    r = Rectangle(12, 7.4)
    print(r.w) # 12, execute the getter method w()
    r.w = 25 # execute the setter method w()
    print(r.w) # 25
    # r_error = Rectangle(-1, 7.4) # ValueError: width or height must be positive
    # r.w = -1 # ValueError: width must be positive

    # check if to rectange objects are equal
    rA = Rectangle(10, 20)
    rB = Rectangle(10, 20)

    # this will print an exception because rA and rB are not the same object
    # they are two different objects with the same values
    if rA is rB:
        print('rA is rB')
    else:
        print('rA is not rB')
    
    # this will not print an exception because rA and rC are the same object
    # changing rC will also change rA
    # rC and rA are references to the same object
    rC = rA
    if rA is rC:
        print('rA is rC')
    else:
        print('rA is not rC')
    
    # this will print an exception because rA and rC are the same object
    # changing rC will also change rA
    # rC and rA are references to the same object
    # but == since that is not the same as 'is'
    # if we add the __eq__ method to the class definition
    # we can compare the values of two objects
    if rA == rB:
        print('rA == rB')
    else:
        print('rA != rB')
    
    # trying to print the rectangle objects, also will print weird hex values
    # because the __str__ method is not implemented
    print(rA) # <__main__.Rectangle object at 0x7f7d1b2c5c10> output without __str__ method
    print(rB) # <__main__.Rectangle object at 0x7f7d1b2c5c40> output without __str__ method

    # Trying to compare two rectangle objects regarding their area
    # if we add the __lt__ method to the class definition
    # we can compare the values of two objects
    if rA < rB:
        print('rA < rB')
    else:
        print('rA >= rB')
    lst = [rA, rB, rC]
    print(sorted(lst)) # [Rectangle(10.00, 20.00), Rectangle(10.00, 20.00), Rectangle(10.00, 20.00)]
    print(sorted(lst, key=lambda r: r.w)) # [Rectangle(10.00, 20.00), Rectangle(10.00, 20.00), Rectangle(10.00, 20.00)]

    # hash the rectangle object
    # if we add the __hash__ method to the class definition
    # we can hash the object
    # object that are equal should have the same hash
    set = {rA, rB, rC}
    print(set) # {Rectangle(10.00, 20.00)}

except ValueError as e:
    print(e)
except Exception as e:
    print(e)
