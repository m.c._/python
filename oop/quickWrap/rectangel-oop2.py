# This example shows how to create a class with instance variables and instance methods.
# It uses type annotations to specify the types of the variables and the return types of the methods.
class Rectangle:
    width: float
    height: float

    # Constructor
    # __init__ is a special method in Python classes, called a constructor.
    def __init__(self, width: float, height: float):
        # self is a reference to the instance of the class.
        # It is used to access variables that belong to the class
        # and to call methods of the class.
        # The width and height variables are instance variables.
        self.width = width
        self.height = height

    # Methods
    # area and perimeter are methods of the Rectangle class.
    # They are functions that belong to the class.
    # They can be called on instances of the class.
    # Therefor they are called instance methods.

    # The area method returns the area of the rectangle.
    def area(self) -> float:
        return self.width * self.height

    # The perimeter method returns the perimeter of the rectangle.
    def perimeter(self) -> float:   
        return 2 * (self.width + self.height)

r1 = Rectangle(10, 20)
r2 = Rectangle(100, 200)
print(r1.area()) # 200
print(r2.area()) # 20000

for r in [r1, r2]:
    print('width', r.width) # 10, 100
    print('height', r.height) # 20, 200
    print('area', r.area()) # 200, 20000
    print('perimeter', r.perimeter()) # 60, 600

# In Python, type annotations are intended for clarity and verification, but they are not used in the language's syntax. 
# They are primarily a feature of some other programming languages. For example, in the code x = 5, 
# the type is not annotated, and you can determine the type using type(x). 
# This example illustrates that Python does not automatically check or process type annotations, 
# unlike languages such as JavaScript or C++.