# Inheritance
import inspect
class A():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def m1(self):
        return self.x + self.y
    
    def m2(self):
        return self.x * self.y
    
class B(A):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z
    
    def m2(self):
        return super().m2() * self.z
    
    def m3(self):
        return self.x + self.y + self.z

a = A(1, 2)
print(a.x, a.y) # 1 2
print(a.m1(), a.m2) # 3 2

b = B(1, 2, 3)
print(b.x, b.y, b.z) # 1 2 3
print(b.m1(), b.m2(), b.m3()) # 3 6 6

# Checking Class Membership and Hierarchy
# isinstance() is used to check if an object is an instance of a class.
print(isinstance(a, A))        # True
print(isinstance(a, B))        # False
print(isinstance(b, A))        # True!
print(isinstance(b, B))        # True

# issubclass() is used to check if a class is a subclass of another class.
print(issubclass(A, B))        # False
print(issubclass(B, A))        # True

# inspect.getmro() is used to get the method resolution order of a class.
print(inspect.getmro(A))
# (<class '__main__.A'>, <class 'object'>)
print(inspect.getmro(B))
# (<class '__main__.B'>, 
#  <class '__main__.A'>, 
#  <class 'object'>)