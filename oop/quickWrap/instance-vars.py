# This file demonstrates the use of instance variables in Python
# Instance variables are variables that belong to instances of a class
# They are declared inside the constructor of the class
# They are accessed using the self keyword
# They are different for each instance of the class

class Sample():
    magicNumber = 42 # this is a class variable

    def __init__(self, somedata, otherdata):
        somevar = 123 # this is a local variable
        self.data = somedata # this is an instance variable
        self.otherdata = otherdata # this is an instance variable

obj1 = Sample(1, 2)
obj2 = Sample(3, 4)

print("Instance variables of obj1: \ndata:",obj1.data,"\notherdata:", obj1.otherdata) # prints 1 2, the instance variables of obj1
print("Instance variables of obj2: \ndata:",obj2.data,"\notherdata:", obj2.otherdata) # prints 3 4, the instance variables of obj2
print("Class variable magicNumber:", Sample.magicNumber) # prints 42, the class variable magicNumber

#namespaces
# The local namespace is the namespace of the function.
# The global namespace is the namespace of the module.
# The class namespace is the namespace of the class.
# The built-in namespace is the namespace of Python's built-in functions.
# The order in which Python looks up names is:
# 1. The local namespace
# 2. The global namespace
# 3. The class namespace
# 4. The built-in namespace

print(vars(obj1)) # prints {'data': 1, 'otherdata': 2}, the instance variables of obj1
print(vars(obj2)) # prints {'data': 3, 'otherdata': 4}, the instance variables of obj2
print(vars(Sample)) # prints {'__module__': '__main__', 'magicNumber': 42, '__init__': <function Sample.__init__ at 0x7f9d0b0b3d30>, '__dict__': <attribute '__dict__' of 'Sample' objects>, '__weakref__': <attribute '__weakref__' of 'Sample' objects>, '__doc__': None}, the class variables of Sample

# What are attributes in Python?
# Attributes are the variables that belong to an object.
# They can be accessed using the dot operator.
# They can be of any type.
# They can be added, modified, or deleted at runtime.

obj1.newdata = 5 # adding a new attribute to obj1
print(vars(obj1)) # prints {'data': 1, 'otherdata': 2, 'newdata': 5}, the instance variables of obj1

# In Python, assigning a value to an instance variable with the same name as a class variable
# creates a new instance variable for that particular instance, rather than modifying the class variable.
# This is different from C#, where modifying a static (class) variable affects all instances of the class.

obj2.magicNumber = 100 # modifying the class variable magicNumber of obj2
print(obj2.magicNumber) # prints 100, the modified class variable magicNumber of obj2
print(Sample.magicNumber) # prints 42, the class variable magicNumber of Sample
