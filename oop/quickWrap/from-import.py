# simple import
import string
import random

# import using from-import
from string import ascii_lowercase
from random import choices

# using the imported modules
def randomstr(n):
    return ''.join(random.choices(string.ascii_lowercase, k=n))

def randomstr2(n):
    return ''.join(choices(ascii_lowercase, k=n))

print(randomstr(10))
print(randomstr2(10))

