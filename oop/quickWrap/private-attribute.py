### Private Instance Variables in Python

# Unlike most other programming languages, Python does not have true private attributes. This means:

# - **No private methods or instance variables**  
#   - It is **impossible** to define variables or methods that are completely inaccessible from outside the class.

# ### Workarounds using Naming Conventions:

# 1. **Single underscore (`_varname`)**  
#    - This is a **convention**, not an enforced rule.  
#    - It **suggests** that the variable or method is **private** and should not be accessed from outside the class.  
#    - However, it is still **accessible** if explicitly referenced.

# 2. **Double underscores (`__varname`) – Name Mangling**  
#    - Python performs **name mangling** by renaming `__varname` to `_ClassName__varname`.  
#    - This makes accidental external access harder, but it is **not true encapsulation**.  
#    - The variable can still be accessed using its mangled name.

# ### Important Notes:
# - These are **conventions**, not strict rules. Developers should respect them but can bypass them if necessary.
# - Python follows a philosophy of "**we are all consenting adults here**"—meaning it relies on discipline rather than enforcement.

class SoCalledPrivate:
    def __init__(self):
        self._private = 42 # single underscore suggests privacy
        self.__really_private = 43 # double underscore suggests name mangling

# But you can still access the so-called private variables
obj = SoCalledPrivate()
print(obj._private) # 42
print(obj._SoCalledPrivate__really_private) # 43 but only if you know the mangled name

# ### Conclusion:
# - Python does not have true private attributes.
# - It relies on naming conventions to suggest privacy.
# - Developers should respect these conventions but can bypass them if necessary.
# - Python follows a philosophy of "**we are all consenting adults here**."
# - For true encapsulation, consider using other techniques or languages.
# - For most purposes, Python's approach is sufficient and effective.
# - It is a trade-off between flexibility and strictness.