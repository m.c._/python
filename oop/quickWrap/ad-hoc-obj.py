class AdHoc():
    pass

obj = AdHoc()
obj.name = "Alice"
obj.age = 30
obj.city = "New York"
# AdHoc class does not have any attributes
# We can add attributes to an object of the class at runtime
# This is an example of ad-hoc object creation

print(vars(obj)) # prints {'name': 'Alice', 'age': 30, 'city': 'New York'}
print(obj.name) # prints Alice
print(obj.age) # prints 30
print(obj.city) # prints New York