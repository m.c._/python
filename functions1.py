##################
#   functions    #
##################

# function without return but input
def greet(name):
    print(f"Hello {name}")

# functions with return and input
def add(a, b):
    return a + b

# read input for greet and use it
name = input("Enter your name: ")
greet(name)

# read input for a and b and use add function
a = int(input("Enter a: "))
b = int(input("Enter b: "))
print(add(a, b))