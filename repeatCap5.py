# some samples for string handling

strWithTicks="strin\"lkdfk"
strWithSlash='kjndfgnnfg\\'
strWithSlash2=r'kjndfgnnfg\r'
print(strWithTicks)
print(strWithSlash)
print(strWithSlash2)

strBla = 'bla [important] bla bla'
start=strBla.find('[')+1
end=strBla.find(']')
print(strBla[start:end])

fileSystem = '/home/mc/pictures/image.jpg'
pos = fileSystem.rfind('/')+1 # rfind returns highest index
path = fileSystem[:pos]
file=fileSystem[pos:]
print('path: ', path, 'file: ', file)

